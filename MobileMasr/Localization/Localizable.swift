//
//  Localizable.swift
//  SAl
//
//  Created by ahmed ezz on 3/11/18.
//  Copyright © 2018 Abdelrahman Ahmed Shawky. All rights reserved.
//

import Foundation
import UIKit


private let appleLanguagesKey = "AppleLanguages"


enum Language: String {
    
    case english = "en"
    case arabic = "ar"
    case ukrainian = "uk"
    
    var semantic: UISemanticContentAttribute {
        switch self {
        case .english, .ukrainian:
            return .forceLeftToRight
        case .arabic:
            return .forceRightToLeft
        }
    }
    

}

//
//extension String {
//
//    var localized: String {
//        return Bundle.localizedBundle.localizedString(forKey: self, value: nil, table: nil)
//    }
//    
//    var localizedImage: UIImage? {
//        return localizedImage()
//            ?? localizedImage(type: ".png")
//            ?? localizedImage(type: ".jpg")
//            ?? localizedImage(type: ".jpeg")
//            ?? UIImage(named: self)
//    }
    
//    private func localizedImage(type: String = "") -> UIImage? {
//        guard let imagePath = Bundle.localizedBundle.path(forResource: self, ofType: type) else {
//            return nil
//        }
//        return UIImage(contentsOfFile: imagePath)
//    }
//}

//extension Bundle {
//    //Here magic happens
//    //when you localize resources: for instance Localizable.strings, images
//    //it creates different bundles
//    //we take appropriate bundle according to language
//    static var localizedBundle: Bundle {
//        //let languageCode = Language.language.rawValue
//        guard let path = Bundle.main.path(forResource: languageCode, ofType: "lproj") else {
//            return Bundle.main
//        }
//        return Bundle(path: path)!
//    }
//}
