//
//  SharedHelper.swift
//  Bj Beauty
//
//  Created by ahmed ezz on 2/18/20.
//  Copyright © 2020 aldar. All rights reserved.
//

import UIKit

class Shared2 {
    
    private let defaults = UserDefaults.standard
    public static var shared = Shared2()
    public func getFromDefault(key:String)->String
    {
        if let value:String  = defaults.string(forKey: key)
        {
            return value
        }
        return ""
    }
    public func saveInDefault(key:String,value:String?)
    {
        defaults.set(value , forKey: key)
        defaults.synchronize()
    }

}
