//
//  LanguageHelper.swift
//  Bj Beauty
//
//  Created by ahmed ezz on 2/18/20.
//  Copyright © 2020 aldar. All rights reserved.
//

import UIKit

class LanguageHelper {
    public static var language = LanguageHelper()
    func currentLanguage() -> String
    {
        let firstLange = Shared2.shared.getFromDefault(key: "language")
        return firstLange.isEmpty ? "en" : firstLange
    }
    func setAppLanguage(lang: String)
    {
        Shared2.shared.saveInDefault(key: "language", value: lang)
    }
    func changeLanguage(lang:String,vc:UIViewController? = nil ,currencyChanged:Bool = false)
    {
        if lang == "ar"
        {
            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceRightToLeft
        }
        else
        {
            UIView.appearance().semanticContentAttribute = UISemanticContentAttribute.forceLeftToRight
        }
        setAppLanguage(lang: lang)
        let mainWindow: UIWindow = UIApplication.shared.windows[0]
        let navigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier:"NavigationController")
        mainWindow.rootViewController = navigationController
        mainWindow.makeKeyAndVisible()
        //mainWindow.backgroundColor = UIColor(hue: 0.6477, saturation: 0.6314, brightness: 0.6077, alpha: 0.8)
        UIView.transition(with: mainWindow, duration: 0.55001, options: .transitionFlipFromLeft, animations: { () -> Void in
        })
        {
            (finished) -> Void in
        }
    }

}
