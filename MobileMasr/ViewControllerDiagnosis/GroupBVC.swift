//
//  GroupBVC.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import UIKit
import CoreLocation
import MBCircularProgressBar
import GoogleMobileAds

class GroupBVC: UIViewController, CLLocationManagerDelegate {
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.layer.borderWidth = 1
            containerView.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            containerView.layer.cornerRadius = 10
            containerView.layer.shadowRadius = 5
            containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            containerView.layer.shadowOpacity = 0.5
        }
    }
    
    @IBOutlet weak var bluetoothLBL: UILabel!
    
    @IBOutlet weak var wifiLBL: UILabel!
    
    @IBOutlet weak var simLBL: UILabel!
    
    @IBOutlet weak var cellularDataLBL: UILabel!
    
    @IBOutlet weak var gpsLBL: UILabel!
    
    @IBOutlet weak var bLBL: UILabel!
    @IBOutlet weak var sLBL: UILabel!
    @IBOutlet weak var wLBL: UILabel!
    @IBOutlet weak var cLBL: UILabel!
    @IBOutlet weak var gLBL: UILabel!
    @IBOutlet weak var titleLBL: UILabel!
    
    
    @IBOutlet weak var prograssBar: MBCircularProgressBarView!
    
    @IBOutlet weak var gifImage: UIImageView!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    let locationManager = CLLocationManager()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = false
//        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
//        navigationItem.leftBarButtonItem = backButton
        let button = UIButton()
        button.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        var image = UIImage(named: "output-onlinepngtools 1")
        image =  image?.resizeImage(CGSize(width: 40, height: 15))
        button.setImage(image, for: .normal)
        button.backgroundColor = .white
        button.isUserInteractionEnabled = false
        button.contentEdgeInsets =  UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        button.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 5)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        self.prograssBar.value = CGFloat(Diagnose.prograssCount)
        Diagnose.GroupBPrograss = self.prograssBar
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        bannerView.rootViewController = self
        
        bannerView.load(GADRequest())
        
        bannerView.delegate = self
        
        locationManager.requestWhenInUseAuthorization()
//        locationManager.requestWhenInUseAuthorization()
//        locationManager.requestLocation()
                navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(addTapped))
        let lang = LocalizationSystem.sharedInstance.getLanguage()
        titleLBL.textAlignment = lang == "ar" ? .right : .left
        wLBL.textAlignment = lang == "ar" ? .right : .left
        bLBL.textAlignment = lang == "ar" ? .right : .left
        sLBL.textAlignment = lang == "ar" ? .right : .left
        cLBL.textAlignment = lang == "ar" ? .right : .left
        gLBL.textAlignment = lang == "ar" ? .right : .left
        bluetoothLBL.textAlignment = lang == "ar" ? .right : .left
        simLBL.textAlignment = lang == "ar" ? .right : .left
        wifiLBL.textAlignment = lang == "ar" ? .right : .left
        cellularDataLBL.textAlignment = lang == "ar" ? .right : .left
        gpsLBL.textAlignment = lang == "ar" ? .right : .left
                navigationItem.hidesBackButton = true
        gifImage.loadGif(name: "animation_300_krkqy2km")
        self.locationManager.delegate = self
        BLEConnection.instance.timesCount = 0
        CheckWifi.instance.timesCount = 0
        CheckCellularData.instance.timesCount = 0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            BLEConnection.instance.startCentralManager(view: self, label: self.bluetoothLBL) {
                SIM.instance.checkSimCard(view: self, label: self.simLBL) {
                    CheckWifi.instance.checkWIFI(view: self, label: self.wifiLBL) {
                        CheckCellularData.instance.checkCellularData(view: self, label: self.cellularDataLBL) {
                            GPS.instance.checkGPS(view: self, locationManager: self.locationManager, label: self.gpsLBL) {
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let vc = storyboard.instantiateViewController(withIdentifier: "GroupCVC") as! GroupCVC
                                self.navigationController?.pushViewController(vc,
                                                                              animated: true)
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    @objc func addTapped () {
        let alert = UIAlertController(title: "CancelDiagnostic".localized, message: "canceltestDiagnostic".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No".localized, style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized, style: UIAlertAction.Style.destructive, handler: {action in
            if BLEConnection.instance.workItem != nil {
                print("hello1")
                DispatchQueue.global().async {
                    BLEConnection.instance.workItem.cancel()
                }
                
            }
            if SIM.instance.workItem != nil {
                print("hello2")
                DispatchQueue.global().async {
                    SIM.instance.workItem.cancel()
                }
                
            }
            if CheckWifi.instance.workItem != nil {
                print("hello3")
                DispatchQueue.global().async {
                    CheckWifi.instance.workItem.cancel()
                }
                
            }
            if CheckCellularData.instance.workItem != nil {
                print("hello4")
                DispatchQueue.global().async {
                    CheckCellularData.instance.workItem.cancel()
                }
                
            }
            if GPS.instance.workItem != nil {
                print("hello5")
                DispatchQueue.global().async {
                    GPS.instance.workItem.cancel()
                }
                
            }
                    let controller = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 5]
                    self.navigationController?.popToViewController(controller!, animated: true)
                }))

                // show the alert
                self.present(alert, animated: true, completion: nil)
        
    }
    
}

extension GroupBVC  {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            print("Location data received.")
            print(location)
        }
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to get users location.")
    }
}
