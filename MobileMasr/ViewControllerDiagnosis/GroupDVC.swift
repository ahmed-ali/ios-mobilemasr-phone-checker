//
//  GroupDVC.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import UIKit
import MediaPlayer
import MBCircularProgressBar
import GoogleMobileAds

class GroupDVC: UIViewController {
    
    
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.layer.borderWidth = 1
            containerView.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            containerView.layer.cornerRadius = 10
            containerView.layer.shadowRadius = 5
            containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            containerView.layer.shadowOpacity = 0.5
        }
    }
    
    @IBOutlet weak var vUPBTNLBL: UILabel!
    
    @IBOutlet weak var vDOWNBTNLBL: UILabel!
    
    @IBOutlet weak var lockBTNLBL: UILabel!
    
    @IBOutlet weak var speakerLBL: UILabel!
    
    @IBOutlet weak var earpieceLBL: UILabel!
    
    @IBOutlet weak var micLBL: UILabel!
    
    @IBOutlet weak var flashLBL: UILabel!
    
    @IBOutlet weak var mLBL: UILabel!
    
    @IBOutlet weak var vLBL: UILabel!
    
    @IBOutlet weak var vdLBL: UIView!
    
    @IBOutlet weak var lLBL: UILabel!
    
    @IBOutlet weak var sLBL: UILabel!
    
    @IBOutlet weak var eLBL: UILabel!
    
    @IBOutlet weak var miLBL: UILabel!
    
    @IBOutlet weak var fLBL: UILabel!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    var fingers = [UITouch?](repeating: nil, count:2)

    
    var volume2:Float! = 0.5

    @IBOutlet weak var porgrassBar: MBCircularProgressBarView!
    
    @IBOutlet weak var gifImage: UIImageView!
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = false

//        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
//        navigationItem.leftBarButtonItem = backButton
        let button = UIButton()
        button.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        var image = UIImage(named: "output-onlinepngtools 1")
        image =  image?.resizeImage(CGSize(width: 40, height: 15))
        button.setImage(image, for: .normal)
        button.backgroundColor = .white
        button.isUserInteractionEnabled = false
        button.contentEdgeInsets =  UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        button.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 5)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)

        MPVolumeView.setVolume(1)
        Diagnose.touchboxesColor = []
        Diagnose.continuousboxesColor = []
        for _ in 0...49{
            Diagnose.touchboxesColor.append(false)
            Diagnose.continuousboxesColor.append(false)
        }
        self.porgrassBar.value = CGFloat(Diagnose.prograssCount)
        Diagnose.GroupDPrograss = self.porgrassBar
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        bannerView.rootViewController = self
        
        bannerView.load(GADRequest())
        bannerView.delegate = self
        
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(addTapped))
        
        let lang = LocalizationSystem.sharedInstance.getLanguage()

        fLBL.textAlignment = lang == "ar" ? .right : .left
        mLBL.textAlignment = lang == "ar" ? .right : .left
        vLBL.textAlignment = lang == "ar" ? .right : .left
        sLBL.textAlignment = lang == "ar" ? .right : .left
        lLBL.textAlignment = lang == "ar" ? .right : .left
        eLBL.textAlignment = lang == "ar" ? .right : .left
        miLBL.textAlignment = lang == "ar" ? .right : .left
        sLBL.textAlignment = lang == "ar" ? .right : .left
        
        vUPBTNLBL.textAlignment = lang == "ar" ? .right : .left
        vDOWNBTNLBL.textAlignment = lang == "ar" ? .right : .left
        lockBTNLBL.textAlignment = lang == "ar" ? .right : .left
        speakerLBL.textAlignment = lang == "ar" ? .right : .left
        earpieceLBL.textAlignment = lang == "ar" ? .right : .left
        micLBL.textAlignment = lang == "ar" ? .right : .left
        flashLBL.textAlignment = lang == "ar" ? .right : .left
        
        
        gifImage.loadGif(name: "animation_300_krkqy2km")
        VolumeUp.instance.timesCount = 0
        VolumeDown.instance.timesCount = 0
        Speaker.instance.timesCount = 0
        EarpiceSpeaker.instance.timesCount = 0
        Flash.instance.timesCount = 0
        VolumeUp.instance.checkVolumeUp(view: self, label: vUPBTNLBL) {
            VolumeDown.instance.checkVolumeDown(view: self, label: self.vDOWNBTNLBL) {
                LockButton.instance.checkLockButton(view: self, label: self.lockBTNLBL) {
                    Speaker.instance.checkSpeaker(view: self, label: self.speakerLBL) {
                        EarpiceSpeaker.instance.checkEarpiceSpeaker(view: self, label: self.earpieceLBL) {
                            Mic.instance.checkMic(view: self, label: self.micLBL) {
                                Flash.instance.checkFlash(view: self, label: self.flashLBL) {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "GroupEVC") as! GroupEVC
                                    self.navigationController?.pushViewController(vc,
                                     animated: true)
                                }
                            }
                        }
                    }
                }
            }
        }
        

    }
    
    

    @objc func addTapped () {
        let alert = UIAlertController(title: "CancelDiagnostic".localized, message: "canceltestDiagnostic".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No".localized, style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized, style: UIAlertAction.Style.destructive, handler: {action in
            if VolumeUp.instance.workItem != nil {
                print("hello1")
                DispatchQueue.global().async {
                    VolumeUp.instance.workItem.cancel()
                }
                
            }
            if VolumeDown.instance.workItem != nil {
                print("hello2")
                DispatchQueue.global().async {
                    VolumeDown.instance.workItem.cancel()
                }
                
            }
            if LockButton.instance.workItem != nil {
                print("hello3")
                DispatchQueue.global().async {
                    LockButton.instance.workItem.cancel()
                }
                
            }
            if Speaker.instance.workItem != nil {
                print("hello4")
                DispatchQueue.global().async {
                    Speaker.instance.workItem.cancel()
                }
                
            }
            if EarpiceSpeaker.instance.workItem != nil {
                print("hello5")
                DispatchQueue.global().async {
                    EarpiceSpeaker.instance.workItem.cancel()
                }
                
            }
            if Mic.instance.workItem != nil {
                print("hello6")
                DispatchQueue.global().async {
                    Mic.instance.workItem.cancel()
                }
                
            }
            if Flash.instance.workItem != nil {
                print("hello7")
                DispatchQueue.global().async {
                    Flash.instance.workItem.cancel()
                }
                
            }
                    let controller = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 7]
                    self.navigationController?.popToViewController(controller!, animated: true)
                }))

                // show the alert
                self.present(alert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
