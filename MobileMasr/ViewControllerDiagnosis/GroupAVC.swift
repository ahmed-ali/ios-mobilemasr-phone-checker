//
//  ViewController3.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import UIKit
import MBCircularProgressBar
import SwiftGifOrigin
import MLKit
import GoogleMobileAds

class GroupAVC: UIViewController {
    
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.layer.borderWidth = 1
            containerView.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            containerView.layer.cornerRadius = 10
            containerView.layer.shadowRadius = 5
            containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            containerView.layer.shadowOpacity = 0.5
            
        }
    }
    @IBOutlet weak var prograssBar: MBCircularProgressBarView!
    
    @IBOutlet weak var PhoneVLBL: UILabel!
    
    @IBOutlet weak var iOSVLBL: UILabel!
    
    @IBOutlet weak var internalSLBL: UILabel!
    
    
    @IBOutlet weak var bLBL: UILabel!
    
    @IBOutlet weak var pLBL: UILabel!
    
    @IBOutlet weak var sLBL: UILabel!
    @IBOutlet weak var iLBL: UILabel!
    
    @IBOutlet weak var gifImage: UIImageView!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    
    var timesCount = 0
    var timesCount2 = 0
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        //        navigationController?.navigationBar.isHidden = true
        
        timesCount = 0
        timesCount2 = 0
        let button = UIButton()
        button.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        var image = UIImage(named: "output-onlinepngtools 1")
        image =  image?.resizeImage(CGSize(width: 40, height: 15))
        button.setImage(image, for: .normal)
        button.backgroundColor = .white
        button.isUserInteractionEnabled = false
        button.contentEdgeInsets =  UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        button.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 5)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        
        
        
        //        navigationItem.rightBarButtonItem = barButton
        
        
        self.prograssBar.value = CGFloat(Diagnose.prograssCount)
//        let backButton = UIBarButtonItem(title: "Back".localized, style: .plain, target: navigationController, action: nil)
//                navigationItem.leftBarButtonItem = backButton
        
        
        Diagnose.GroupAPrograss = self.prograssBar
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        bannerView.rootViewController = self
        
        bannerView.load(GADRequest())
        
        bannerView.delegate = self
        
        
                navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(addTapped))
        let lang = LocalizationSystem.sharedInstance.getLanguage()
        pLBL.textAlignment = lang == "ar" ? .right : .left
        iLBL.textAlignment = lang == "ar" ? .right : .left
        bLBL.textAlignment = lang == "ar" ? .right : .left
        sLBL.textAlignment = lang == "ar" ? .right : .left
        PhoneVLBL.textAlignment = lang == "ar" ? .right : .left
        iOSVLBL.textAlignment = lang == "ar" ? .right : .left
        internalSLBL.textAlignment = lang == "ar" ? .right : .left
        navigationItem.hidesBackButton = true
        gifImage.loadGif(name: "animation_300_krkqy2km")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            //            Phone Version
            PhoneVersion.instance.phoneVersion(view: self, label: self.PhoneVLBL) {
                //                iOS Version
                IOSVersion.instance.iOSVersion(view: self, label: self.iOSVLBL) {
                    //                    Internal Storage
                    Storage.instance.checkStorage(view: self, label: self.internalSLBL) {

                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let vc = storyboard.instantiateViewController(withIdentifier: "GroupBVC") as! GroupBVC

                        self.navigationController?.pushViewController(vc,
                                                                      animated: true)
                    }
                }
            }
          
            
            
            
        }
        
    }
    
    
    
    @objc func addTapped () {
        // create the alert
       
        
        
        
        let alert = UIAlertController(title: "CancelDiagnostic".localized, message: "canceltestDiagnostic".localized, preferredStyle: UIAlertController.Style.alert)
        
        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No".localized, style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized, style: UIAlertAction.Style.destructive, handler: {action in
            
            if PhoneVersion.instance.workItem != nil {
                print("hello1")
                DispatchQueue.global().async {
                    PhoneVersion.instance.workItem.cancel()
                }
                
            }
            if IOSVersion.instance.workItem != nil {
                print("hello2")
                DispatchQueue.global().async {
                IOSVersion.instance.workItem.cancel()
                }
            }
            if Storage.instance.workItem != nil {
                print("hello3")
                DispatchQueue.global().async {
                Storage.instance.workItem.cancel()
                }
            }
            
           
            
            let controller = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 4]
            self.navigationController?.popToViewController(controller!, animated: true)
        }))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
}
