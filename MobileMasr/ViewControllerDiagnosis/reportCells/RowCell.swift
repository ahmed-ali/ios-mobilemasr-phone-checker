//
//  RowCell.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 27/06/2021.
//

import UIKit

class RowCell: UITableViewCell {

    @IBOutlet weak var titleLBL: UILabel!{
        didSet{
            titleLBL.numberOfLines = 1
        }
    }
    
    @IBOutlet weak var descLBL: UILabel!{
        didSet{
            descLBL.numberOfLines = 0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
