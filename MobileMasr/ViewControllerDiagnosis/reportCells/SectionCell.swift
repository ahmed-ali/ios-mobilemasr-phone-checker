//
//  SectionCell.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 27/06/2021.
//

import UIKit

class SectionCell: UITableViewCell {
    @IBOutlet weak var sensName: UILabel!
    @IBOutlet weak var btnSelection: UIButton!
    @IBOutlet weak var isWorkedImage: UIImageView!
    @IBOutlet weak var menuImg: UIImageView!
    @IBOutlet weak var sensImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
