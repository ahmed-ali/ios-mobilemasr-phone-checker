//
//  AdCell.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 09/09/2021.
//

import UIKit
import GoogleMobileAds

class AdCell: UITableViewCell {

    @IBOutlet weak var bannerView: GADBannerView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        bannerView.load(GADRequest())
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
