//
//  ReportVC.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 27/06/2021.
//

import UIKit
import GoogleMobileAds

class ReportVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var generatePDFBTNOutlet: UIButton!{
        didSet{
            generatePDFBTNOutlet.layer.cornerRadius = generatePDFBTNOutlet.bounds.height / 2
        }
    }
    
    @IBOutlet weak var percentLBL: UILabel!{
        didSet{
            percentLBL.layer.cornerRadius = 10
            
            percentLBL.layer.masksToBounds = true
        }
    }
    @IBOutlet weak var conditionLBL: UILabel!{
        didSet{
            conditionLBL.layer.cornerRadius = 10
            conditionLBL.layer.masksToBounds = true
        }
    }
    
    var selectedIndx = -1
    
    var thereIsCellTapped = false
    
    lazy var adBannerView: GADBannerView = {
        let adBannerView = GADBannerView(adSize: kGADAdSizeBanner)
        adBannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        adBannerView.rootViewController = self
        
        return adBannerView
    }()
    
    lazy var adBannerView2: GADBannerView = {
        let adBannerView2 = GADBannerView(adSize: kGADAdSizeBanner)
        adBannerView2.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        adBannerView2.rootViewController = self
        
        return adBannerView2
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = false
        //
        //        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
        //        navigationItem.leftBarButtonItem = backButton
        let button = UIButton()
        button.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        var image = UIImage(named: "output-onlinepngtools 1")
        image =  image?.resizeImage(CGSize(width: 40, height: 15))
        button.setImage(image, for: .normal)
        button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        button.isUserInteractionEnabled = false
        button.contentEdgeInsets =  UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        button.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 5)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        navigationController?.navigationBar.isHidden = false
        navigationController?.navigationItem.hidesBackButton = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //        ads
        adBannerView.load(GADRequest())
        adBannerView2.load(GADRequest())
        let translateTransform = CGAffineTransform(translationX: 0, y: -adBannerView.bounds.size.height)
        adBannerView.transform = translateTransform
        let translateTransform2 = CGAffineTransform(translationX: 0, y: -adBannerView2.bounds.size.height)
        adBannerView.transform = translateTransform2
        UIView.animate(withDuration: 0.5) {
            self.tableView.tableHeaderView?.frame = self.adBannerView.frame
            self.tableView.tableFooterView?.frame = self.adBannerView2.frame
            self.tableView.tableHeaderView = self.adBannerView
            self.tableView.tableFooterView = self.adBannerView2
            self.adBannerView.transform = CGAffineTransform.identity
            self.adBannerView2.transform = CGAffineTransform.identity
        }
        
        
        tableView.separatorStyle = .singleLine
        tableView.separatorColor = .darkGray
        
        if Diagnose.finalResultPrograss > 0 && Diagnose.finalResultPrograss <= 25 {
            percentLBL.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            percentLBL.text = " " + " \(Diagnose.finalResultPrograss ) % " + " "
            conditionLBL.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            conditionLBL.text = "Ma2bool"
        }
        else if Diagnose.finalResultPrograss > 25 && Diagnose.finalResultPrograss <= 50 {
            percentLBL.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            percentLBL.text = "\(Diagnose.finalResultPrograss ) %"
            conditionLBL.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            conditionLBL.text = "Good"
            
        }
        else if Diagnose.finalResultPrograss > 50 && Diagnose.finalResultPrograss <= 75 {
            percentLBL.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            percentLBL.text = "\(Diagnose.finalResultPrograss ) %"
            conditionLBL.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            conditionLBL.text = "Very Good"
            
        }
        else if Diagnose.finalResultPrograss > 75 && Diagnose.finalResultPrograss <= 100{
            percentLBL.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            percentLBL.text = "\(Diagnose.finalResultPrograss ) %"
            conditionLBL.backgroundColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            conditionLBL.text = "Excellent"
        }
        
        tableView.register(UINib(nibName: "RowCell", bundle: nil), forCellReuseIdentifier: "RowCell")
        tableView.register(UINib(nibName: "SectionCell", bundle: nil), forCellReuseIdentifier: "SectionCell")
        tableView.register(UINib(nibName: "AdCell", bundle: nil), forCellReuseIdentifier: "AdCell")
        tableView.reloadData()
    }
    
    
    @IBAction func generatePDFBTNPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "PDFVC") as! PDFVC
        self.navigationController?.pushViewController(vc,
         animated: true)
    }
    
    
}


extension ReportVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return Diagnose.finalReportResult.count + 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section > 13 {
            if Diagnose.finalReportResult[section - 1].is_worked == true && Diagnose.finalReportResult[section - 1].data?.count ?? 0 > 0  {
                return Diagnose.finalReportResult[section - 1].data?.count ?? 0
            }
            else{
                return 0
            }
        }
        else if section == 13{
            return 0
        }
        else{
            if Diagnose.finalReportResult[section].is_worked == true && Diagnose.finalReportResult[section].data?.count ?? 0 > 0  {
                return Diagnose.finalReportResult[section].data?.count ?? 0
            }
            else{
                return 0
            }
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RowCell") as! RowCell
        if indexPath.section > 13 {
            cell.titleLBL.text = Diagnose.finalReportResult[indexPath.section - 1].data?[indexPath.row].name
            
            
            cell.descLBL.text = Diagnose.finalReportResult[indexPath.section - 1].data?[indexPath.row].desc
        }
        else {
            cell.titleLBL.text = Diagnose.finalReportResult[indexPath.section].data?[indexPath.row].name
            
            
            cell.descLBL.text = Diagnose.finalReportResult[indexPath.section ].data?[indexPath.row].desc
        }
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 13 {
            return 50
        }
        else{
            return 45
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == selectedIndx && thereIsCellTapped{
            return UITableView.automaticDimension
        }else{
            return 0
        }
    }
    //    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    //        // UIView with darkGray background for section-separators as Section Footer
    //        let v = UIView(frame: CGRect(x: 0, y:0, width: tableView.frame.width, height: 0.5))
    //        v.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
    //        return v
    //    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        // Section Footer height
        
        return 0
        
        
    }
    
    //    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    //        let adCell = tableView.dequeueReusableCell(withIdentifier: "AdCell") as! AdCell
    //        switch section {
    //        case 0,13,28:
    //            adCell.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
    //            adCell.bannerView.rootViewController = self
    //
    //            adCell.bannerView.delegate = self
    //            return adCell
    //        default:
    //            return adCell
    //        }
    //
    //
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath.section)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "SectionCell") as! SectionCell
        if section > 13{
            headerCell.sensImage.image = Diagnose.finalReportResult[section - 1].image
            headerCell.sensName.text =  Diagnose.finalReportResult[section - 1].name
            if (Diagnose.finalReportResult[section - 1].is_worked == true){
                headerCell.isWorkedImage.image = UIImage(named: "check")
            }
            else{
                headerCell.isWorkedImage.image = UIImage(named: "x-mark")
            }
            
            if Diagnose.finalReportResult[section - 1].data?.count ?? 0 > 0 {
                headerCell.menuImg.isHidden = false
            }
            else{
                headerCell.menuImg.isHidden = true
            }
            headerCell.btnSelection.tag = section
            headerCell.btnSelection.addTarget(self, action: #selector(self.btnSectionClick(sender:)), for: .touchUpInside)
            return headerCell
        }
        else if section == 13 {
            let adCell = tableView.dequeueReusableCell(withIdentifier: "AdCell") as! AdCell
            adCell.bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
            adCell.bannerView.rootViewController = self
            
            adCell.bannerView.delegate = self
            return adCell
        }
        else{
            headerCell.sensImage.image = Diagnose.finalReportResult[section].image
            headerCell.sensName.text =  Diagnose.finalReportResult[section].name
            if (Diagnose.finalReportResult[section].is_worked == true){
                headerCell.isWorkedImage.image = UIImage(named: "check")
            }
            else{
                headerCell.isWorkedImage.image = UIImage(named: "x-mark")
            }
            
            if Diagnose.finalReportResult[section].data?.count ?? 0 > 0 {
                headerCell.menuImg.isHidden = false
            }
            else{
                headerCell.menuImg.isHidden = true
            }
            headerCell.btnSelection.tag = section
            headerCell.btnSelection.addTarget(self, action: #selector(self.btnSectionClick(sender:)), for: .touchUpInside)
            return headerCell
        }
        
        
    }
    
    
    //    private func tableView(tableView: UITableView,
    //                            willDisplayCell cell: UITableViewCell,
    //                            forRowAtIndexPath indexPath: NSIndexPath)
    //    {
    //            let additionalSeparatorThickness = CGFloat(3)
    //        let additionalSeparator = UIView(frame: CGRect(x: 0,
    //                                                       y: cell.frame.size.height - additionalSeparatorThickness,
    //                                                       width: cell.frame.size.width,
    //                                                       height: additionalSeparatorThickness))
    //            additionalSeparator.backgroundColor = UIColor.darkGray
    //            cell.addSubview(additionalSeparator)
    //    }
    
    @objc func btnSectionClick(sender:UIButton!){
        print("selected index",sender.tag)
        if selectedIndx != sender.tag {
            self.thereIsCellTapped = true
            self.selectedIndx = sender.tag
        }
        else {
            // there is no cell selected anymore
            self.thereIsCellTapped = false
            self.selectedIndx = -1
        }
        tableView.reloadData()
    }
    
    
    
}


//extension ReportVC: GADBannerViewDelegate {
//
//    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
//        print("Banner loaded successfully")
//        w
//    }
//
//    adview
//}
