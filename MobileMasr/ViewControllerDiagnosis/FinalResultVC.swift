//
//  FinalResultVC.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import UIKit
import MBCircularProgressBar
import GoogleMobileAds

class FinalResultVC: UIViewController {

    @IBOutlet weak var diagProgressView: MBCircularProgressBarView!
    
    @IBOutlet weak var diagnosResultLBL: UILabel!
    
    @IBOutlet weak var sellBTNOutlet: UIButton!{
        didSet{
            sellBTNOutlet.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 25)
            sellBTNOutlet.layer.shadowRadius = 5
            sellBTNOutlet.layer.shadowOffset = CGSize(width: 0, height: 2)
            sellBTNOutlet.layer.shadowOpacity = 0.5
        }
    }
    
    @IBOutlet weak var againBTNOutlet: UIButton!{
        didSet{
            againBTNOutlet.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 25)
            againBTNOutlet.layer.shadowRadius = 5
            againBTNOutlet.layer.shadowOffset = CGSize(width: 0, height: 2)
            againBTNOutlet.layer.shadowOpacity = 0.5
        }
    }
    @IBOutlet weak var bannerView: GADBannerView!
    
    private var interstitial: GADInterstitialAd?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.isHidden = false
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(addTapped))
        let button = UIButton()
        button.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        var image = UIImage(named: "output-onlinepngtools 1")
        image =  image?.resizeImage(CGSize(width: 40, height: 15))
        button.setImage(image, for: .normal)
        button.backgroundColor = .white
        button.contentEdgeInsets =  UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        button.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 5)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
//        navigationController?.navigationItem.hidesBackButton = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        bannerView.rootViewController = self
        
        bannerView.load(GADRequest())
        bannerView.delegate = self
        
        let request = GADRequest()
            GADInterstitialAd.load(withAdUnitID:"ca-app-pub-3940256099942544/4411468910",
                                        request: request,
                              completionHandler: { [self] ad, error in
                                if let error = error {
                                  print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                  return
                                }
                                interstitial = ad
                              }
            )
        
        self.diagProgressView.value = 0
        for result in Diagnose.result {
            if result == true {
                self.diagProgressView.value += 4
                Diagnose.finalResultPrograss += 4
            }
        }
        
        for resultscore in Diagnose.resultScorePoint{
            self.diagProgressView.value += resultscore
            Diagnose.finalResultPrograss += resultscore
        }
        
        diagnosResultLBL.text = "diagnsoeResult".localized + UIDevice.modelName
        
        
//        MPVolumeView.setVolume(0.5)
       
    }
    

    @objc func addTapped () {
        let alert = UIAlertController(title: "CancelDiagnostic".localized, message: "canceltestDiagnostic".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No".localized, style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized, style: UIAlertAction.Style.destructive, handler: {action in
            
                    let controller = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 9]
                    self.navigationController?.popToViewController(controller!, animated: true)
                }))

                // show the alert
                self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func startAgainBTNPressed(_ sender: UIButton) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "NavigationController")
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: true, completion: nil)
        if interstitial != nil {
            interstitial?.present(fromRootViewController: self)
          } else {
            print("Ad wasn't ready")
          }
        let controller = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 8]
        self.navigationController?.popToViewController(controller!, animated: true)
        

    }
    
    @IBAction func finalReportBTNPressed(_ sender: UIButton) {
        if interstitial != nil {
            interstitial?.present(fromRootViewController: self)
          } else {
            print("Ad wasn't ready")
          }
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ReportVC") as! ReportVC
        self.navigationController?.pushViewController(vc,
         animated: true)
    }
    
    
    
}
