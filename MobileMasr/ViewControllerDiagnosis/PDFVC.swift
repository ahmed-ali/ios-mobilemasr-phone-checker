//
//  PDFVC.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 26/09/2021.
//

import UIKit
import WebKit
import PDFKit
import SCLAlertView

struct PDFData {
    let name : String
    let vlaue: String
}

class PDFVC: UIViewController, WKNavigationDelegate {
    

    @IBOutlet weak var webView: WKWebView!
    
    
    let diagnose = Diagnose.finalReportResult
        var data = [PDFData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        for _ in 0...26 {
            let pdfData = PDFData(name: "hello", vlaue: "true")
            data.append(pdfData)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            print(self.data)
            self.createPDF()
        }
        
    }
    
    func createPDF() {
        let html = "<html><body><ul>    <li>" + data[0].name + "<svg style='width:24px;height:24px' viewBox='0 0 24 24'>    <path fill='green' d='M12 2C6.5 2 2 6.5 2 12S6.5 22 12 22 22 17.5 22 12 17.5 2 12 2M10 17L5 12L6.41 10.59L10 14.17L17.59 6.58L19 8L10 17Z' /></svg></li></ul></body></html>"
        let render = UIPrintPageRenderer()
           let fmt = UIMarkupTextPrintFormatter(markupText: html)

        render.addPrintFormatter(fmt, startingAtPageAt: 0)

           // 3. Assign paperRect and printableRect

           let page = CGRect(x: 0, y: 0, width: 595.2, height: 841.8) // a4 size
           let printable = page.insetBy(dx: 0, dy: 0)

        render.setValue(NSValue(cgRect: page), forKey: "paperRect")
        render.setValue(NSValue(cgRect: printable), forKey: "printableRect")

           // 4. Create PDF context and draw
           //let pointzero = CGPoint(x: 0,y :0)
//           let rect = CGRect.zero
           let pdfData = NSMutableData()
           UIGraphicsBeginPDFContextToData(pdfData, .zero, nil)

           for i in 0..<render.numberOfPages {
               UIGraphicsBeginPDFPage();
            render.drawPage( at: i, in: UIGraphicsGetPDFContextBounds())
           }

           UIGraphicsEndPDFContext();

           // 5. Save PDF file
          let path = "\(NSTemporaryDirectory())sd.pdf"

        pdfData.write( toFile: path, atomically: true)
           print("open \(path)")
        loadPDF(filename: "sd", path: "\(NSTemporaryDirectory())")
    }

    
    func loadPDF(filename: String , path:String) {
        let url = URL(fileURLWithPath: path, isDirectory: true).appendingPathComponent(filename).appendingPathExtension("pdf")
        let urlRequest = URLRequest(url: url)
        webView.load(urlRequest)
    }
    
    
    @IBAction func shareButton(_ sender: Any) {
//        let path = Bundle.main.path(forResource: "sd",  ofType:"pdf")
//                let fileURL = URL(fileURLWithPath: "\(NSTemporaryDirectory())sd.pdf")
//
//                // Create the Array which includes the files you want to share
//                var filesToShare = [Any]()
//
//                // Add the path of the file to the Array
//                filesToShare.append(fileURL)
//
//                // Make the activityViewContoller which shows the share-view
//                let activityViewController = UIActivityViewController(activityItems: filesToShare, applicationActivities: nil)
//
//                // Show the share-view
//                self.present(activityViewController, animated: true, completion: nil)
        
        // Add a text field
        let appearance = SCLAlertView.SCLAppearance(
            kWindowWidth: self.view.bounds.width * 0.7, showCloseButton: false,
            hideWhenBackgroundViewIsTapped: true
        )
        let alert = SCLAlertView(appearance: appearance)
        let alertViewIcon = UIImage(named: "email")
        let txt = alert.addTextField("Enter your first name")
        let txt2 = alert.addTextField("Enter your last name")
        let txt3 = alert.addTextField("Enter your email")
        let txt4 = alert.addTextField("Enter your phone")
        alert.addButton("Send") {
            
        }
        alert.showWarning("Send Report", subTitle: "Please fill form to send report", colorStyle: 0x0D1724, colorTextButton: 0xFFFFFF, circleIconImage: alertViewIcon)
        
      
    }
    
}



