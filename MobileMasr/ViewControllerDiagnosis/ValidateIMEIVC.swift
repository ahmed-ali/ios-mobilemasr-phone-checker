//
//  ValidateIMEIVC.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 03/07/2021.
//

import UIKit
import SkyFloatingLabelTextField
import AVKit
import AVFoundation
import GoogleMobileAds


class ValidateIMEIVC: UIViewController {
    
    //MARK: Outlets
    @IBOutlet weak var nextBTNOutlet: UIButton!{
        didSet{
            nextBTNOutlet.layer.cornerRadius = 15
        }
    }
    
    @IBOutlet weak var showVideoBTNOutlet: UIButton!
    
    @IBOutlet weak var imeiTEXTField: SkyFloatingLabelTextFieldWithIcon!{
        didSet {
            imeiTEXTField.keyboardType = .asciiCapableNumberPad
            imeiTEXTField.placeholder = "EnterIMEI".localized
        }
    }
    
    @IBOutlet weak var fLBL: UILabel!
    
    @IBOutlet weak var oLBL: UILabel!
    
    @IBOutlet weak var gLBL: UILabel!
    
    @IBOutlet weak var aLBL: UILabel!
    
    @IBOutlet weak var sLBL: UILabel!
    
    @IBOutlet weak var stackView: UIView!{
        didSet{
            stackView.layer.cornerRadius = 10
            stackView.layer.shadowColor = UIColor.black.cgColor
            stackView.layer.shadowOffset = CGSize(width: 0, height: 0)
            stackView.layer.shadowOpacity = 0.3
            stackView.layer.shadowRadius = 4.0
        }
    }

//    ads
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet var banner2view: GADBannerView!
    
    //MARK: Constants
    private var interstitial: GADInterstitialAd?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let button = UIButton()
        button.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        var image = UIImage(named: "output-onlinepngtools 1")
        image =  image?.resizeImage(CGSize(width: 40, height: 15))
        button.setImage(image, for: .normal)
        button.backgroundColor = .white
        button.isUserInteractionEnabled = false
        button.contentEdgeInsets =  UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        button.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 5)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        navigationController?.navigationBar.isHidden = false
        
        Diagnose.prograssCount = 0
        Diagnose.result = []
        Diagnose.resultScorePoint = []
        Diagnose.finalReportResult = []
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        ads
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        bannerView.rootViewController = self
        
        banner2view.adUnitID = "ca-app-pub-3940256099942544/2934735716"

        banner2view.rootViewController = self
        
        bannerView.load(GADRequest())
        
        banner2view.load(GADRequest())
        
        let request = GADRequest()
            GADInterstitialAd.load(withAdUnitID:"ca-app-pub-3940256099942544/4411468910",
                                        request: request,
                              completionHandler: { [self] ad, error in
                                if let error = error {
                                  print("Failed to load interstitial ad with error: \(error.localizedDescription)")
                                  return
                                }
                                interstitial = ad
                              }
            )
          

        
        
//      Button enabled
        nextBTNOutlet.isEnabled = false
        
//      localization setting for page
        let lang = LocalizationSystem.sharedInstance.getLanguage()
        showVideoBTNOutlet.contentHorizontalAlignment = lang == "ar" ? .right : .left
        fLBL.textAlignment = lang == "ar" ? .right : .left
        oLBL.textAlignment = lang == "ar" ? .right : .left
        gLBL.textAlignment = lang == "ar" ? .right : .left
        aLBL.textAlignment = lang == "ar" ? .right : .left
        sLBL.textAlignment = lang == "ar" ? .right : .left
        
//      IMEI Seter
        self.imeiTEXTField.title = "IMEI"
        
        self.imeiTEXTField.errorColor = UIColor.red
        
        self.imeiTEXTField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }
    
    
    //MARK: Helper Functions
    @objc func textFieldDidChange(_ textfield: SkyFloatingLabelTextField) {
        if let text = imeiTEXTField.text {
            if let floatingLabelTextField = imeiTEXTField {
                if text.count < 15 || text.count > 15 {
                    floatingLabelTextField.errorMessage = "InvalidIMEI".localized
                    nextBTNOutlet.isEnabled = false
                    self.imeiTEXTField.iconImage = .none
                    self.imeiTEXTField.lineColor = UIColor.red
                }
                else{
                    if self.luhnCheck(text) {
                        floatingLabelTextField.errorMessage = ""
                        nextBTNOutlet.isEnabled = true
                        self.imeiTEXTField.titleColor = UIColor.green
                        self.imeiTEXTField.iconType = .image
                        self.imeiTEXTField.iconImage = UIImage(imageLiteralResourceName: "checked")
                        if #available(iOS 13.0, *) {
                            self.imeiTEXTField.scalesLargeContentImage = true
                        } else {
                            // Fallback on earlier versions
                        }
                        self.imeiTEXTField.selectedLineColor = UIColor.green
                        self.imeiTEXTField.titleColor = UIColor.green

                        }
                    else {
                        floatingLabelTextField.errorMessage = "InvalidIMEI".localized
                        nextBTNOutlet.isEnabled = false
                        self.imeiTEXTField.iconImage = .none
                        self.imeiTEXTField.lineColor = UIColor.red

                        // The error message will only disappear when we reset it to nil or empty string
                    }
                }
                
            }
        }
    }
    
    func luhnCheck(_ number: String) -> Bool {
        var sum = 0
        let digitStrings = number.reversed().map { String($0) }
        
        for tuple in digitStrings.enumerated() {
            if let digit = Int(tuple.element) {
                let odd = tuple.offset % 2 == 1
                
                switch (odd, digit) {
                case (true, 9):
                    sum += 9
                case (true, 0...8):
                    sum += (digit * 2) % 9
                default:
                    sum += digit
                }
            } else {
                return false
            }
        }
        return sum % 10 == 0
    }
    
    
    //MARK: IBActions
    @IBAction func watchVideoBTNPressed(_ sender: UIButton) {
        guard let path = Bundle.main.path(forResource: "IMEI", ofType:"MP4") else {
            debugPrint("IMEI.MP4 not found")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
    @IBAction func nextBTNPressed(_ sender: UIButton) {
        if interstitial != nil {
            interstitial?.present(fromRootViewController: self)
          } else {
            print("Ad wasn't ready")
          }
        Diagnose.finalReportResult.append(Result(name: "IMEI", is_worked: true, image: nil, data: [DataResult(name: "Code".localized, desc: self.imeiTEXTField.text)]))
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewController2") as! ViewController2
        self.navigationController?.pushViewController(vc,
         animated: true)
    }
    
    
    
}


