//
//  GroupEVC.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import UIKit
import MBCircularProgressBar
import GoogleMobileAds

class GroupEVC: UIViewController {
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.layer.borderWidth = 1
            containerView.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            containerView.layer.cornerRadius = 10
            containerView.layer.shadowRadius = 5
            containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            containerView.layer.shadowOpacity = 0.5
        }
    }
    
    @IBOutlet weak var cancelBTNOutlet: UIButton!{
        didSet{
            cancelBTNOutlet.isHidden = false
        }
    }
    
    @IBOutlet weak var d3LBL: UILabel!
    
    @IBOutlet weak var multitouchLBL: UILabel!
    
    @IBOutlet weak var contTouchLBL: UILabel!
    
    @IBOutlet weak var rearCamLBL: UILabel!
    
    @IBOutlet weak var frontCamLBL: UILabel!
    
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var mLBL: UILabel!
    
    
    @IBOutlet weak var cLBL: UILabel!
    @IBOutlet weak var dLBL: UILabel!
    
    @IBOutlet weak var fLBL: UILabel!
    @IBOutlet weak var rLBL: UILabel!
    
    @IBOutlet weak var prograssBar: MBCircularProgressBarView!
    
    @IBOutlet weak var gifImage: UIImageView!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    
//    let textDetector = GMVDetector.init(ofType: GMVDetectorTypeText, options: nil)
//    let options = [
//        GMVDetectorFaceLandmarkType: GMVDetectorFaceLandmark.self,//GMVDetectorFaceLandmarkAll
//        GMVDetectorFaceClassificationType: GMVDetectorFaceClassification.self,//GMVDetectorFaceClassificationAll
//        GMVDetectorFaceTrackingEnabled: NSNumber(value: false)
//    ] as [String : Any]
    
    
    var timesCount = 0
    var timesCount2 = 0
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = true


        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        bannerView.rootViewController = self
        
        bannerView.load(GADRequest())
        bannerView.delegate = self
        
        
        self.prograssBar.value = CGFloat(Diagnose.prograssCount)
        Diagnose.GroupEPrograss = self.prograssBar
        let lang = LocalizationSystem.sharedInstance.getLanguage()

        fLBL.textAlignment = lang == "ar" ? .right : .left
        mLBL.textAlignment = lang == "ar" ? .right : .left
        cLBL.textAlignment = lang == "ar" ? .right : .left
        titleLBL.textAlignment = lang == "ar" ? .right : .left
        rLBL.textAlignment = lang == "ar" ? .right : .left
        titleLBL.textAlignment = lang == "ar" ? .right : .left
        dLBL.textAlignment = lang == "ar" ? .right : .left
        
        multitouchLBL.textAlignment = lang == "ar" ? .right : .left
        contTouchLBL.textAlignment = lang == "ar" ? .right : .left
        d3LBL.textAlignment = lang == "ar" ? .right : .left
        rearCamLBL.textAlignment = lang == "ar" ? .right : .left
        frontCamLBL.textAlignment = lang == "ar" ? .right : .left
        
        //        
//                navigationItem.hidesBackButton = false
        cancelBTNOutlet.setTitle("Cancel".localized, for: .normal)
        gifImage.loadGif(name: "animation_300_krkqy2km")
        Touch.instance.timesCount = 0
        ContTouch.instance.timesCount = 0
        D3Touch.instance.timesCount = 0
        timesCount = 0
        timesCount2 = 0
        
        
        Touch.instance.checkTouch(view: self, mainlabel: self.multitouchLBL) {
            ContTouch.instance.checkTouch(view: self, mainlabel: self.contTouchLBL) {
                D3Touch.instance.checkTouch(view: self, mainlabel: self.d3LBL) {
                    BackCamer.instance.checkCamera(view: self, label: self.rearCamLBL) {
                        FrontCamer.instance.checkFrontCamer(view: self, label: self.frontCamLBL) {
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let vc = storyboard.instantiateViewController(withIdentifier: "FinalResultVC") as! FinalResultVC
                            self.navigationController?.pushViewController(vc,
                             animated: true)
                        }
                    }
                }
                
            }
            
        }
            
        
       
    }
    @IBAction func cancelBTNPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "CancelDiagnostic".localized, message: "canceltestDiagnostic".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No".localized, style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized, style: UIAlertAction.Style.destructive, handler: {action in
            if Touch.instance.workItem != nil {
                print("hello5")
                DispatchQueue.global().async {
                    Touch.instance.workItem.cancel()
                }
                
            }
            if ContTouch.instance.workItem != nil {
                print("hello5")
                DispatchQueue.global().async {
                    ContTouch.instance.workItem.cancel()
                }
                
            }
            if D3Touch.instance.workItem != nil {
                print("hello5")
                DispatchQueue.global().async {
                    D3Touch.instance.workItem.cancel()
                }
                
            }
            if BackCamer.instance.workItem != nil {
                print("hello5")
                DispatchQueue.global().async {
                    BackCamer.instance.workItem.cancel()
                }
                
            }
            if FrontCamer.instance.workItem != nil {
                print("hello5")
                DispatchQueue.global().async {
                    FrontCamer.instance.workItem.cancel()
                }
                
            }
                    let controller = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 8]
                    self.navigationController?.popToViewController(controller!, animated: true)
                }))

                // show the alert
                self.present(alert, animated: true, completion: nil)
    }
    
   

}
