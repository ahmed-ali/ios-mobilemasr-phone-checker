//
//  GroupCVC.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import UIKit
import MBCircularProgressBar
import GoogleMobileAds

class GroupCVC: UIViewController {
    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.layer.borderWidth = 1
            containerView.layer.borderColor = #colorLiteral(red: 0.9294117647, green: 0.3803921569, blue: 0.03921568627, alpha: 1)
            containerView.layer.cornerRadius = 10
            containerView.layer.shadowRadius = 5
            containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            containerView.layer.shadowOpacity = 0.5
        }
    }
    
    @IBOutlet weak var memoryLBL: UILabel!
    @IBOutlet weak var thermalLBL: UILabel!
    @IBOutlet weak var batlvlLBL: UILabel!
    @IBOutlet weak var batStateLBL: UILabel!
    @IBOutlet weak var vibrateLBL: UILabel!
    @IBOutlet weak var idLBL: UILabel!
    @IBOutlet weak var accLBL: UILabel!
    
    @IBOutlet weak var mLBL: UILabel!
    @IBOutlet weak var tLBL: UILabel!
    @IBOutlet weak var vLBL: UILabel!
    @IBOutlet weak var sLBL: UILabel!
    @IBOutlet weak var vibLBL: UILabel!
    @IBOutlet weak var iLBL: UILabel!
    @IBOutlet weak var aLBL: UILabel!
    @IBOutlet weak var titleLBL: UILabel!
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    @IBOutlet weak var prograssBar: MBCircularProgressBarView!
    
    @IBOutlet weak var gifImage: UIImageView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.isNavigationBarHidden = false

//        let backButton = UIBarButtonItem(title: "", style: .plain, target: navigationController, action: nil)
//        navigationItem.leftBarButtonItem = backButton
        let button = UIButton()
        button.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        var image = UIImage(named: "output-onlinepngtools 1")
        image =  image?.resizeImage(CGSize(width: 40, height: 15))
        button.setImage(image, for: .normal)
        button.backgroundColor = .white
        button.isUserInteractionEnabled = false
        button.contentEdgeInsets =  UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        button.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 5)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        self.prograssBar.value = CGFloat(Diagnose.prograssCount)
        Diagnose.GroupCPrograss = self.prograssBar
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        bannerView.rootViewController = self
        
        bannerView.load(GADRequest())
        bannerView.delegate = self
        
        let lang = LocalizationSystem.sharedInstance.getLanguage()

        titleLBL.textAlignment = lang == "ar" ? .right : .left
        mLBL.textAlignment = lang == "ar" ? .right : .left
        vLBL.textAlignment = lang == "ar" ? .right : .left
        sLBL.textAlignment = lang == "ar" ? .right : .left
        vibLBL.textAlignment = lang == "ar" ? .right : .left
        iLBL.textAlignment = lang == "ar" ? .right : .left
        aLBL.textAlignment = lang == "ar" ? .right : .left
        sLBL.textAlignment = lang == "ar" ? .right : .left
        tLBL.textAlignment = lang == "ar" ? .right : .left

        memoryLBL.textAlignment = lang == "ar" ? .right : .left
        thermalLBL.textAlignment = lang == "ar" ? .right : .left
        batlvlLBL.textAlignment = lang == "ar" ? .right : .left
        batStateLBL.textAlignment = lang == "ar" ? .right : .left
        vibrateLBL.textAlignment = lang == "ar" ? .right : .left
        idLBL.textAlignment = lang == "ar" ? .right : .left
        accLBL.textAlignment = lang == "ar" ? .right : .left
                navigationItem.hidesBackButton = true
                navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel".localized, style: .plain, target: self, action: #selector(addTapped))
                navigationItem.hidesBackButton = true
        gifImage.loadGif(name: "animation_300_krkqy2km")
        BatteryState.instance.timesCount = 0
        Vibrate.instance.timesCount = 0
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            Memory.instance.checkMemory(view: self, label: self.memoryLBL) {
                Thermal.instance.checkThermal(view: self, label: self.thermalLBL) {
                    BatteryLVL.instance.checkBatteryLVL(view: self, label: self.batlvlLBL) {
                        BatteryState.instance.checkBatteryState(view: self, label: self.batStateLBL) {
                            Vibrate.instance.checkVibrate(view: self, label: self.vibrateLBL) {
                                ID.instance.checkID(view: self, label: self.idLBL) {
                                    Accele.instance.checkAccele(view: self, label: self.accLBL) {
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        let vc = storyboard.instantiateViewController(withIdentifier: "GroupDVC") as! GroupDVC
                                        self.navigationController?.pushViewController(vc,
                                         animated: true)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        
    }
    
    
    @objc func addTapped () {
        let alert = UIAlertController(title: "CancelDiagnostic".localized, message: "canceltestDiagnostic".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "No".localized, style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Yes".localized, style: UIAlertAction.Style.destructive, handler: {action in
            if Memory.instance.workItem != nil {
                print("hello1")
                DispatchQueue.global().async {
                    Memory.instance.workItem.cancel()
                }
                
            }
            if Thermal.instance.workItem != nil {
                print("hello2")
                DispatchQueue.global().async {
                    Thermal.instance.workItem.cancel()
                }
                
            }
            if BatteryLVL.instance.workItem != nil {
                print("hello3")
                DispatchQueue.global().async {
                    BatteryLVL.instance.workItem.cancel()
                }
                
            }
            if BatteryState.instance.workItem != nil {
                print("hello4")
                DispatchQueue.global().async {
                    BatteryState.instance.workItem.cancel()
                }
                
            }
            if Vibrate.instance.workItem != nil {
                print("hello5")
                DispatchQueue.global().async {
                    Vibrate.instance.workItem.cancel()
                }
                
            }
            if ID.instance.workItem != nil {
                print("hello6")
                DispatchQueue.global().async {
                    ID.instance.workItem.cancel()
                }
                
            }
            if Accele.instance.workItem != nil {
                print("hello7")
                DispatchQueue.global().async {
                    Accele.instance.workItem.cancel()
                }
                
            }
                    let controller = self.navigationController?.viewControllers[(self.navigationController?.viewControllers.count)! - 6]
                    self.navigationController?.popToViewController(controller!, animated: true)
                }))

                // show the alert
                self.present(alert, animated: true, completion: nil)
    }
}
