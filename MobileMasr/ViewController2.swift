//
//  ViewController2.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/05/2021.
//

import UIKit
import GoogleMobileAds
class ViewController2: UIViewController {

    @IBOutlet weak var containerView: UIView!{
        didSet{
            containerView.layer.borderWidth = 1
            containerView.layer.borderColor = #colorLiteral(red: 0.05098039216, green: 0.09019607843, blue: 0.1411764706, alpha: 1)
            containerView.layer.cornerRadius = 10
            containerView.layer.shadowRadius = 5
            containerView.layer.shadowOffset = CGSize(width: 0, height: 2)
            containerView.layer.shadowOpacity = 0.5
            
        }
    }
    @IBOutlet weak var followingTitle: UILabel!
    
    @IBOutlet weak var startBTNOutlet: UIButton!{
        didSet{
            startBTNOutlet.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: Float(startBTNOutlet.bounds.height) / 2)
            startBTNOutlet.layer.shadowRadius = 5
            startBTNOutlet.layer.shadowOffset = CGSize(width: 0, height: 2)
            startBTNOutlet.layer.shadowOpacity = 0.5
        }
    }
    
    @IBOutlet weak var bannerView: GADBannerView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716"
        
        bannerView.rootViewController = self
        
        bannerView.load(GADRequest())
        bannerView.delegate = self
        
        
        let lang = LocalizationSystem.sharedInstance.getLanguage()
        containerView.semanticContentAttribute = lang == "ar" ? .forceRightToLeft : .forceLeftToRight
        followingTitle.textAlignment = lang == "ar" ? .right : .left
        // Do any additional setup after loading the view.
        
        startBTNOutlet.setTitle("startDiagnosetest".localized + UIDevice.modelName, for: .normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = false
    }


}

extension ViewController2: UITableViewDelegate, UITableViewDataSource {
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 27
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        let lang = LocalizationSystem.sharedInstance.getLanguage()
        cell.nameLBL.textAlignment = lang == "ar" ? .right : .left
        switch indexPath.row {
        case 0:
            cell.nameLBL.text = "1. " + "PhoneVersion".localized
            return cell
        case 1:
            cell.nameLBL.text = "2. " + "iOSVersion".localized
            return cell
        case 2:
            cell.nameLBL.text = "3. " + "InternalStorage".localized
            return cell
        case 3:
            cell.nameLBL.text = "4. " + "Memory".localized
            return cell
        case 4:
            cell.nameLBL.text = "5. " + "Thermal".localized
            return cell
        case 5:
            cell.nameLBL.text = "6. " + "SIM".localized
            return cell
        case 6:
            cell.nameLBL.text = "7. " + "Bluetooth".localized
            return cell
        case 7:
            cell.nameLBL.text = "8. " + "Wifi".localized
            return cell
        case 8:
            cell.nameLBL.text = "9. " + "GPS".localized
            return cell
        case 9:
            cell.nameLBL.text = "10. " + "BatteryLevel".localized
            return cell
        case 10:
            cell.nameLBL.text = "11. " + "BatteryState".localized
            return cell
        case 11:
            cell.nameLBL.text = "12. " + "CellularData".localized
            return cell
        case 12:
            cell.nameLBL.text = "13. " + "Speaker".localized
            return cell
        case 13:
            cell.nameLBL.text = "14. " + "EarpieceSpeaker".localized
            return cell
        case 14:
            cell.nameLBL.text = "15. " + "Mic".localized
            return cell
        case 15:
            cell.nameLBL.text = "16. " + "Vibrate".localized
            return cell
        case 16:
            cell.nameLBL.text = "17. " + "Flash".localized
            return cell
        case 17:
            cell.nameLBL.text = "18. " + "Accelerometer".localized
            return cell
        case 18:
            cell.nameLBL.text = "19. " + "VolumeUpButton".localized
            return cell
        case 19:
            cell.nameLBL.text = "20. " + "VolumeDownButton".localized
            return cell
        case 20:
            cell.nameLBL.text = "21. " + "LockButton".localized
            return cell
        case 21:
            cell.nameLBL.text = "22. " + "MultipleTouch".localized
            return cell
        case 22:
            cell.nameLBL.text = "23. " + "3DTouch".localized
            return cell
        case 23:
            cell.nameLBL.text = "24. " + "ContinuousTouch".localized
            return cell
        case 24:
            cell.nameLBL.text = "25. " + "ID".localized
            return cell
        case 25:
            cell.nameLBL.text = "26. " + "BackCamera".localized
            return cell
        default:
            cell.nameLBL.text = "27. " + "FrontCamera".localized
            return cell
        }
    }
}
