//
//  AppDelegate.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import UIKit
import IQKeyboardManagerSwift
import MOLH
import GoogleMobileAds
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        MOLH.shared.activate(true)
        Localizer.DoTheSwizzling()
        //        registerforDeviceLockNotification()
        KeyChain.checkID()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.overrideKeyboardAppearance = true
        IQKeyboardManager.shared.keyboardAppearance = .light
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
        
    }
    
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        DispatchQueue.main.async {
            if (self.isScreenLocked) {
                print("pressed")
                if Diagnose.lockButton {
                    print("Sent to background by locking screen")
                    LockButton.instance.checkDone()
                }
            } else {
                print("LOCK STATUS CHANGED")
            }
        }
        
    }
    var isScreenLocked: Bool {
        return UIScreen.main.brightness == 0.0
    }
    
    
    
}


