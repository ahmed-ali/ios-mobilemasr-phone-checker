//
//  ViewController.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/05/2021.
//

import UIKit
import MOLH
import ContextMenuSwift

class ViewController: UIViewController {
    
    
    //MARK: Outlets
    
    @IBOutlet weak var linkBTNOutlet: UIButton!{
        didSet{
            
        }
    }
    

    @IBOutlet weak var langBTNOutlet: UIButton!{
        didSet{
            langBTNOutlet.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 5)
        }
    }
    
    @IBOutlet weak var testBTNOutlet: UIButton!{
        didSet{
            testBTNOutlet.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 25)
            testBTNOutlet.layer.shadowRadius = 5
            testBTNOutlet.layer.shadowOffset = CGSize(width: 0, height: 3)
            testBTNOutlet.layer.shadowOpacity = 0.5
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
            //            langBTNOutlet.setTitle("Arabic", for: .normal)
            let myNormalAttributedTitle = NSAttributedString(string: "العربية",
                                                             attributes: [NSAttributedString.Key.underlineStyle: 1 , NSAttributedString.Key.font: UIFont(name: "Arial", size: 15.0)!, NSAttributedString.Key.foregroundColor: UIColor.themeColor])
            langBTNOutlet.setAttributedTitle(myNormalAttributedTitle, for: .normal)
            langBTNOutlet.setImage(UIImage(named: "egypt"), for: .normal)
            langBTNOutlet.semanticContentAttribute = .forceRightToLeft
            langBTNOutlet.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: -10)
        }
        else{
            let myNormalAttributedTitle = NSAttributedString(string: "EN",
                                                             attributes: [NSAttributedString.Key.underlineStyle: 1 , NSAttributedString.Key.font: UIFont(name: "Arial", size: 15.0)!, NSAttributedString.Key.foregroundColor: UIColor.themeColor])
            langBTNOutlet.setAttributedTitle(myNormalAttributedTitle, for: .normal)
            langBTNOutlet.setImage(UIImage(named:"america"), for: .normal)
            langBTNOutlet.semanticContentAttribute = .forceLeftToRight
            langBTNOutlet.imageEdgeInsets = UIEdgeInsets(top: 0, left: -10, bottom: 0, right: 0)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    //    @IBAction func switchLanguage(_ sender: Any) {
    //        switchLanguag()
    //    }
    
    
    @IBAction func switchPRessed(_ sender: Any) {
        //        if switchOutlet.isOn {
        //            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
        //            LanguageHelper.language.changeLanguage(lang: "ar")
        //
        //
        //        }
        //        else{
        //            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
        //            LanguageHelper.language.changeLanguage(lang: "en")
        //
        //
        //        }
    }
    
    func switchLanguag() {
        
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            LanguageHelper.language.changeLanguage(lang: "en")
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
            
            //UIView.appearance().semanticContentAttribute = .forceLeftToRight
        } else {
            LanguageHelper.language.changeLanguage(lang: "ar")
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
            
        }
        //        print(UserDefaults.standard.array(forKey: "AppleLanguages"))
        //        print(LanguageManger.shared.currentLanguage)
    }
    
    @IBAction func linkBTNPressed(_ sender: UIButton) {
        //        https://mobitech.com.eg/
        //        verifyUrl(urlString: "https://mobitech.com.eg/")
        if let url = URL(string: "https://mobitech.com.eg/"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    func verifyUrl (urlString: String?) -> Bool {
        if let urlString = urlString {
            if let url  = NSURL(string: urlString) {
                return UIApplication.shared.canOpenURL(url as URL)
            }
        }
        return false
    }
    
    
 
    
  
    
    @IBAction func langBTNPressed(_ sender: UIButton) {
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            LanguageHelper.language.changeLanguage(lang: "en")
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
        } else {
            LanguageHelper.language.changeLanguage(lang: "ar")
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
            
        }
        //        CM.items = ["English", "Arabic"]
        //        CM.showMenu(viewTargeted: langBTNOutlet, delegate: self, animated: true)
    }
}



extension ViewController : ContextMenuDelegate {
    func contextMenuDidSelect(_ contextMenu: ContextMenu, cell: ContextMenuCell, targetedView: UIView, didSelect item: ContextMenuItem, forRowAt index: Int) -> Bool {
        if index == 0 {
            LanguageHelper.language.changeLanguage(lang: "en")
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
        } else {
            LanguageHelper.language.changeLanguage(lang: "ar")
            LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
            
        }
        return true
    }
    
    func contextMenuDidDeselect(_ contextMenu: ContextMenu, cell: ContextMenuCell, targetedView: UIView, didSelect item: ContextMenuItem, forRowAt index: Int) {
        
        return
    }
    
    func contextMenuDidAppear(_ contextMenu: ContextMenu) {
        
    }
    
    func contextMenuDidDisappear(_ contextMenu: ContextMenu) {
        
    }
    
    
}
extension UIColor {
    static let themeColor = UIColor(red: 13.0/255.0, green: 23.0/255.0, blue: 36.0/255.0, alpha: 1.0)
}
