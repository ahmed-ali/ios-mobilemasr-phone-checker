//
//  SceneDelegate.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import UIKit
import MOLH
import GoogleMobileAds
@available(iOS 13.0, *)
class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?


    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        GADMobileAds.sharedInstance().start(completionHandler: nil)
        MOLH.shared.activate(true)
        Localizer.DoTheSwizzling()
        KeyChain.checkID()
//        registerforDeviceLockNotification()
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
//        if Diagnose.lockButton {
//            print("Sent to background by locking screen")
//            LockButton.instance.checkDone()
//        }
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        DispatchQueue.main.async {
            if (self.isScreenLocked) {
                print("pressed")
                if Diagnose.lockButton {
                    print("Sent to background by locking screen")
                    LockButton.instance.checkDone()
                }
            } else {
                print("LOCK STATUS CHANGED")
            }
        }
        
    }
    var isScreenLocked: Bool {
        return UIScreen.main.brightness == 0.0
    }
    



}

