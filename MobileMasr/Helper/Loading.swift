//
//  Loading.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 19/08/2021.
//

import Foundation
import NVActivityIndicatorView
import KRProgressHUD

class Loading {
    let shared = Loading()
    var activity = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), type: .ballClipRotate, color: .white, padding: 0)
    func stratLoading (view: UIViewController) {
        activity.center = view.view.center
        activity.startAnimating()
    }
    
    func successLoading() {
        activity.stopAnimating()
    }
    
    func fieldLoading(errorMessage: String){
        activity.stopAnimating()
        KRProgressHUD.showError(withMessage: errorMessage)
    }
}
