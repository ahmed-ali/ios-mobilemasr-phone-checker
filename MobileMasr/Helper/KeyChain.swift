//
//  KeyChain.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 30/08/2021.
//


import UIKit
import KeychainSwift

class KeyChain {
    
    

    class func checkID() {
        let keychain = KeychainSwift()
        if let receivedData = keychain.get("UUID") {
            print("get KeyChain")
            print("result: ", receivedData)
            UserDefaults.standard.setValue(receivedData, forKey: "UUID")
            UserDefaults.standard.synchronize()
        }
        else{
            let UUID = UUID().uuidString
            keychain.set(UUID, forKey: "UUID")
            UserDefaults.standard.setValue(UUID, forKey: "UUID")
            UserDefaults.standard.synchronize()
            print("set KeyChain")
        }
    }
}
