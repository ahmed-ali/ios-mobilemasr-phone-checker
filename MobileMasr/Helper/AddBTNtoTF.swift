//
//  AddBTNtoTF.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 19/08/2021.
//

import Foundation
import UIKit

class addBTNToTF: UITextField {
    
    static let addBTNN = addBTNToTF()
    var iconClick = true
    var TextField : UITextField? = nil
    var arr : [UITextField] = []
    let BTN = UIButton(type: .custom)
    var buttonTint : UIColor?
//    var image  = UIImage(named: "visibleeye")
    func addBTNToTextField(image: String, TF: [UITextField], bTint: UIColor) {
        for i in TF {
            arr = TF
           TextField = i
            buttonTint = bTint
            BTN.tag = i.tag
            let ImageView = UIImage(named: image)
            let mainView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 45))
            mainView.layer.cornerRadius = 5
            BTN.setImage(ImageView, for: .normal)
            BTN.tintColor = bTint
            BTN.frame = CGRect(x: 12.0, y: 10.0, width: 24.0, height: 24.0)
            BTN.addTarget(self, action: #selector(self.ShowPassword(_:)), for: .touchUpInside)
            mainView.addSubview(BTN)
            i.rightView = mainView
            i.rightViewMode = .always
        }
    }
    
    @objc func ShowPassword(_ sender: UIButton){
        let tf = arr.filter{$0.tag == sender.tag}.first ?? UITextField()
        tf.isSecureTextEntry = !tf.isSecureTextEntry
        if tf.isSecureTextEntry {
            BTN.setImage(UIImage(named: "visibleeye"), for: .normal)
        }
        else{
            BTN.setImage(UIImage(named: "visibility"), for: .normal)
        }
        BTN.tintColor = buttonTint
    }
    
    
    func removeBTN () {
        BTN.removeFromSuperview()
    }
}

