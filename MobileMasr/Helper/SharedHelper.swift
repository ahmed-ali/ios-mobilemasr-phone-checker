//
//  SharedHelper.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 19/08/2021.
//

import Foundation
class SharedHelper {
    static let shared = SharedHelper()
    let userdefaulte = UserDefaults.standard
    
    func save(key: String ,value: Any ) {
        userdefaulte.set(value, forKey: key)
        userdefaulte.synchronize()
    }
    
    func remove(key: String){
        userdefaulte.removeObject(forKey: key)
        userdefaulte.synchronize()
    }
    
    func get(key: String){
        userdefaulte.object(forKey: key)
        userdefaulte.synchronize()
    }
    
}
