//
//  InstructionDialogVC.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 27/07/2021.
//

import UIKit

class InstructionDialogVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!{
        didSet{
            tableView.layer.borderWidth = 1
            tableView.layer.borderColor = #colorLiteral(red: 0.05098039216, green: 0.09019607843, blue: 0.1411764706, alpha: 1)
            tableView.layer.cornerRadius = 10
            tableView.layer.shadowRadius = 5
            tableView.layer.shadowOffset = CGSize(width: 0, height: 2)
            tableView.layer.shadowOpacity = 0.5
            
        }
    }
    
    @IBOutlet weak var instLBL: UILabel!
    @IBOutlet weak var nextBTNOutlet: UIButton!{
        didSet{
            nextBTNOutlet.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 25)
            nextBTNOutlet.layer.shadowRadius = 5
            nextBTNOutlet.layer.shadowOffset = CGSize(width: 0, height: 3)
            nextBTNOutlet.layer.shadowOpacity = 0.5
        }
    }
    
   
    
    let bullet = "•  "
            
    var strings = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let lang = LocalizationSystem.sharedInstance.getLanguage()
        instLBL.textAlignment = lang == "ar" ? .right : .left
        
        strings.append("Cleanthefrontandbackcamerawell.".localized)
        strings.append("Cleanthescreenwell.".localized)
        strings.append("Cleanthechargerportthoroughly".localized)
        strings.append("Cleantheearpiecespeaker,speaker,andmicwell.".localized)
        strings.append("keepawayfromthenoise.".localized)
        strings.append("Ifthemobilehasafingerprint,pleasecleanitwell.".localized)
        strings.append("Followtheinstructionsduringthediagnosis.".localized)
        strings = strings.map { return bullet + $0 }
        
        tableView.reloadData()
        tableView.allowsSelection = false
        
//        instructionLBL.attributedText = htmlString.htmlToAttributedString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let button = UIButton()
        button.frame = CGRect(x: 0.0, y: 0.0, width: 20, height: 20)
        var image = UIImage(named: "output-onlinepngtools 1")
        image =  image?.resizeImage(CGSize(width: 40, height: 15))
        button.setImage(image, for: .normal)
        button.backgroundColor = .white
        button.isUserInteractionEnabled = false
        button.contentEdgeInsets =  UIEdgeInsets(top: 2, left: 5, bottom: 2, right: 5)
        button.setBorderUIView(color: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), borderWidth: 0, cornerRadius: 5)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
        navigationController?.navigationBar.isHidden = false
    }
    


    
    

}


extension InstructionDialogVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "instratcionCell") as! instratcionCell
        var attributes = [NSAttributedString.Key: Any]()
                attributes[.font] = UIFont.preferredFont(forTextStyle: .body)
                attributes[.foregroundColor] = UIColor.darkGray
                
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.headIndent = (bullet as NSString).size(withAttributes: attributes).width
                attributes[.paragraphStyle] = paragraphStyle
        let string = strings.joined(separator: "\n\n")
        cell.instructionLBL.attributedText = NSAttributedString(string: string, attributes: attributes)
        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
            cell.instructionLBL.textAlignment = .right
        }
        else{
            cell.instructionLBL.textAlignment = .left
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.bounds.height
    }
}
