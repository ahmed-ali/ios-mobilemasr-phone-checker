//
//  Touch.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import Foundation
import UIKit

class ContTouch {
    
    private init() {
        
    }
    
    static let instance = ContTouch()
    
    var application: UIApplication?
    var collectionViewGest: UICollectionView?
    var timesCount = 0
    var viewC: UIViewController!
    var view2: UIView!
    var completion: (() -> Void)!
    var label : UILabel!
    var workItem: DispatchWorkItem!
    var completed : Bool! = false
    func checkTouch(view:UIViewController, mainlabel:UILabel,completion: @escaping ()->Void) {
        self.completed = false
        self.completion = completion
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            mainlabel.addRunning(view: view)
            self.viewC = view
            // create the alert
            let alert = UIAlertController(title: "TestContinuousTouch".localized, message: "ContTouchMessage".localized, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: { action in
                if self.workItem.isCancelled {
                    return
                }
                let myView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width
                                                  , height: UIScreen.main.bounds.height
                ))
                self.view2 = myView
                myView.backgroundColor = .white
                view.view.addSubview(myView)
                
                let label = UILabel()
                label.textAlignment = NSTextAlignment.center
                label.text = ""
                
                
                
                
                
                var secondsRemaining = 20
                let layout = UICollectionViewFlowLayout()
                layout.sectionInset = UIEdgeInsets(top:0, left: 0, bottom: 0, right: 0)
                layout.minimumInteritemSpacing = 0
                layout.minimumLineSpacing = 0
                
                layout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
                let collectionView = UICollectionView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), collectionViewLayout: layout)
                collectionView.alwaysBounceVertical = false
                
                self.collectionViewGest = collectionView
                self.label = mainlabel
                
                self.setupLongGestureRecognizerOnCollection()
                
//                self.completion = completion
                collectionView.backgroundColor = .red
                collectionView.tag = 1
                collectionView.register(UINib(nibName: "TouchCell", bundle: .main), forCellWithReuseIdentifier: "touchCell")
                collectionView.contentInsetAdjustmentBehavior = .never
                collectionView.delegate = view as? UICollectionViewDelegate
                collectionView.dataSource = view as? UICollectionViewDataSource
                Diagnose.collection = collectionView
                myView.addSubview(collectionView)
                Diagnose.collection = collectionView
                label.frame = CGRect(x: 10,y: (UIScreen.main.bounds.height - 20 - 10),width: 100,height: 20);
                
                myView.addSubview(label)
                
                Diagnose.continuoustouchView = myView
                
                
                
                
                var timer = Timer()
                
                timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (Timer) in
                    Diagnose.timer = Timer
                    if secondsRemaining > 0 {
                        label.text = "\(secondsRemaining)" + "second".localized
                        //                        print ("\(self.secondsRemaining) seconds")
                        secondsRemaining -= 1
                    } else {
                        Timer.invalidate()
                        print("score == 4 normal")
                        self.onDidReceiveData()
                        self.completed = true
                    }
                }
                
                Diagnose.timer = timer
                timer.fire()
                
                Diagnose.funcContTouch = self.onDidReceiveData
                
                
                
                
                
            }))
            
            // show the alert
            view.present(alert, animated: true, completion: nil)
        }
   
        DispatchQueue.main.async(execute: self.workItem)
        
        
    }
    
    
     func onDidReceiveData() {
        guard self.completed == false else {
            return
        }
        self.collectionViewGest?.removeFromSuperview()
        self.view2.removeFromSuperview()
        let score = (Diagnose.continuousTouchCount / 100) * 4
        print(score)
        if score == 4 {
            self.completed = true
            if self.workItem.isCancelled {
                return
            }
            print("score == 4")
            Diagnose.GroupEPrograss?.value += 8
            Diagnose.finalReportResult.append(Result(name: "ContinuousTouch".localized, is_worked: true, image: UIImage(named: "touch-screen"), data: [DataResult(name: "ScorePoint".localized, desc: String(Diagnose.continuousTouchCount) + "%")]))
            
            Diagnose.resultScorePoint.append(CGFloat(score))
            
            self.label.addText(view: self.viewC, text: String(Diagnose.continuousTouchCount) + "%")
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
        else{
            self.completed = true
            print("else")
            if self.timesCount < 2 {
                if self.workItem.isCancelled {
                    return
                }
                let alert = UIAlertController(title: "TestContinuousTouch".localized, message: "ContTouchMessage2".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {_ in
                    if self.workItem.isCancelled {
                        return
                    }
                 DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                     Diagnose.GroupEPrograss?.value += 8
                     let score = (Diagnose.continuousTouchCount / 100) * 4
                     
                     
                    Diagnose.finalReportResult.append(Result(name: "ContinuousTouch".localized, is_worked: true, image: UIImage(named: "touch-screen"), data: [DataResult(name: "ScorePoint".localized, desc: String(Diagnose.continuousTouchCount) + "%")]))
                     
                     Diagnose.resultScorePoint.append(CGFloat(score))
                    self.label.addText(view: self.viewC, text: String(Diagnose.continuousTouchCount) + "%")
                    if self.workItem.isCancelled {
                        return
                    }
                     DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.completion()
                     }
                 }
                }))
                alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.cancel, handler: {_ in
                    if self.workItem.isCancelled {
                        return
                    }
                 Diagnose.touchboxesColor = []
                 for _ in 0...49{
                     Diagnose.continuousboxesColor.append(false)
                     
                 }
                 Diagnose.touchCount = 0
                    self.checkTouch(view: self.viewC, mainlabel: self.label) {
                    self.completion()
                 }
                 
                }))

                // show the alert
                self.viewC.present(alert, animated: true, completion: nil)
                self.timesCount += 1
            }
            else{
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    Diagnose.GroupEPrograss?.value += 8
                    let score = (Diagnose.continuousTouchCount / 100) * 4
                    
                    
                    Diagnose.finalReportResult.append(Result(name: "ContinuousTouch".localized, is_worked: true, image: UIImage(named: "touch-screen"), data: [DataResult(name: "ScorePoint".localized, desc: String(Diagnose.continuousTouchCount) + "%")]))
                    
                    Diagnose.resultScorePoint.append(CGFloat(score))
                    self.label.addText(view: self.viewC, text: String(Diagnose.continuousTouchCount) + "%")
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.completion()
                    }
                }
            }
            
        }
        
          
    }
    
    //MARK: Contuinus touch
    func setupLongGestureRecognizerOnCollection(){
        let doubleTap: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(didDoubleTap))
//            doubleTap.numberOfTapsRequired = 1
        self.collectionViewGest?.addGestureRecognizer(doubleTap)
    }
    @objc func didDoubleTap(gesture: UIPanGestureRecognizer) {

        let point: CGPoint = gesture.location(in: self.collectionViewGest)

        print(point)

        if let selectedIndexPath: IndexPath = self.collectionViewGest?.indexPathForItem(at: point) {
            
            Diagnose.continuousboxesColor[selectedIndexPath.item] = true
            var count = 0
            
            for i in 0...49 {
                if (Diagnose.continuousboxesColor[i]) == true {
                    count += 2
                }
            }
            
            Diagnose.continuousTouchCount = count
            if Diagnose.continuousboxesColor.contains(false){
                self.collectionViewGest?.reloadData()
            }
            else{
                Diagnose.continuoustouchView?.removeFromSuperview()
                Diagnose.timer?.invalidate()
                Diagnose.funcContTouch!()

            }
        }
    }
    
    //MARK: LongPressTouch
//    private func setupLongGestureRecognizerOnCollection() {
//        self.collectionViewGest?.canCancelContentTouches = false
//        self.collectionViewGest?.allowsMultipleSelection = true
//        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
//        longPressedGesture.minimumPressDuration = 0.1
//        longPressedGesture.delegate = self.viewC as? UIGestureRecognizerDelegate
//        longPressedGesture.delaysTouchesBegan = true
//        self.collectionViewGest?.addGestureRecognizer(longPressedGesture)
//    }
//
//    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
//        if (gestureRecognizer.state != .began) {
//            return
//        }
//
//        let p = gestureRecognizer.location(in: self.collectionViewGest)
//
//        if let indexPath = self.collectionViewGest?.indexPathForItem(at: p) {
//            print("Long press at item: \(indexPath.row)")
//        }
//    }
    
}

extension GroupEVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIGestureRecognizerDelegate {
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
            return Diagnose.continuousboxesColor.count
        
        
       
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "touchCell", for: indexPath) as! TouchCell
        
        
            if (Diagnose.continuousboxesColor[indexPath.item]) {
                cell.cellBackGround.backgroundColor = .green
                
            }
            else{
                cell.cellBackGround.backgroundColor = .red
            }
        
        
        
        
        cell.cellBackGround.layer.borderWidth = 0.5
        cell.cellBackGround.layer.borderColor = #colorLiteral(red: 0, green: 0.2159909904, blue: 0.3851273656, alpha: 1)
        return cell
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       
        
            Diagnose.continuousboxesColor[indexPath.item] = true
            var count = 0
            
            for i in 0...49 {
                if (Diagnose.continuousboxesColor[i]) == true {
                    count += 2
                }
            }
            
            Diagnose.continuousTouchCount = count
            if Diagnose.continuousboxesColor.contains(false){
                collectionView.reloadData()
            }
            else{
                print("google")
                Diagnose.continuoustouchView?.removeFromSuperview()
                Diagnose.timer?.invalidate()

            }
            
        
       
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = collectionView.bounds.width / 5
        let cellHeight = collectionView.bounds.height / 10
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    
    
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    
    
}

