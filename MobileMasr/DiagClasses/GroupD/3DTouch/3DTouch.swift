//
//  3DTouch.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 08/07/2021.
//

import Foundation
//
//  Touch.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import Foundation
import UIKit
import Pulsator
class D3Touch{
    
    private init() {
        
    }
    
    static let instance = D3Touch()
    
    var application: UIApplication?
    var collectionViewGest: UICollectionView?
    var timesCount = 0
    var viewC: UIViewController?
    var longPressButton: UIButton?
    let pulsator = Pulsator()
    let buttonCancel:UIButton = UIButton(frame: CGRect(x: 40, y: 100, width: 20, height: 20))
    var Label : UILabel!
    var completion: (() -> Void)!
    var myView : UIView?
    var workItem: DispatchWorkItem!
    func checkTouch(view:UIViewController, mainlabel:UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            mainlabel.addRunning(view: view)
            self.viewC = view
            self.completion = completion
            self.Label = mainlabel
            if self.workItem.isCancelled {
                return
            }
            // create the alert
            let alert = UIAlertController(title: "Test3DTouch".localized, message: "3DTouchMessage".localized, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: { action in
                if self.workItem.isCancelled {
                    return
                }
                let myView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width
                                                  , height: UIScreen.main.bounds.height
                ))
                myView.backgroundColor = .white
                self.myView = myView
                view.view.addSubview(myView)
                
                self.longPressButton = UIButton(frame: CGRect(x: 0, y: 0, width: 120, height: 120))
                let btnHeight = self.longPressButton?.bounds.height
                self.longPressButton?.setBorderUIView(color: #colorLiteral(red: 0, green: 0.3411764706, blue: 0.4117647059, alpha: 1), borderWidth: 1, cornerRadius: Float(btnHeight ?? 120) / 2)
                self.longPressButton?.setTitle("LongPress".localized, for: .normal)
                self.longPressButton?.titleLabel?.lineBreakMode = NSLineBreakMode.byWordWrapping
                self.longPressButton?.contentHorizontalAlignment = .center
                self.longPressButton?.setTitleColor(#colorLiteral(red: 0, green: 0.3411764706, blue: 0.4117647059, alpha: 1), for: .normal)
                self.buttonCancel.backgroundColor = .clear
                self.buttonCancel.setImage(UIImage(named: "cancel"), for: .normal)
                self.buttonCancel.addTarget(self, action:#selector(self.buttonCancelClicked), for: .touchUpInside)
                myView.addSubview(self.buttonCancel)
                myView.addSubview(self.longPressButton!)
                self.longPressButton?.center = myView.center
                
                self.setupLongGestureRecognizerOnCollection()
                
                
                
            }))
            
            // show the alert
            view.present(alert, animated: true, completion: nil)
        }
        
        DispatchQueue.main.async(execute: self.workItem)
        
    }
    

    
    //MARK: LongPressTouch
    private func setupLongGestureRecognizerOnCollection() {
        if self.workItem.isCancelled {
            return
        }
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressedGesture.minimumPressDuration = 1
        longPressedGesture.delegate = self.viewC as? UIGestureRecognizerDelegate
        longPressedGesture.delaysTouchesBegan = true
        self.longPressButton?.addGestureRecognizer(longPressedGesture)
        if self.workItem.isCancelled {
            return
        }
    }
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if self.workItem.isCancelled {
            return
        }
        if (gestureRecognizer.state != .began) {
            print("!= .began")
            if self.workItem.isCancelled {
                return
            }
            self.longPressButton!.layer.insertSublayer(pulsator, at: 0)
            let buttonMidX = self.longPressButton!.bounds.midX
            let buttonMidY = self.longPressButton!.bounds.midY
                let buttonBoundsCenter = CGPoint(x: buttonMidX, y: buttonMidY)
            
            pulsator.backgroundColor = #colorLiteral(red: 0, green: 0.3411764706, blue: 0.4117647059, alpha: 1)
                // 3. set the backgroundLayer's postion to the buttonBoundsCenter
            pulsator.position = buttonBoundsCenter
            pulsator.numPulse = 3
            pulsator.radius = 150

            pulsator.start()
            if self.workItem.isCancelled {
                return
            }
            
            return
        }
    
       
        
//        let pulsator = Pulsator()
        self.myView?.removeFromSuperview()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
            
            self.Label.addSuccess(view: self.viewC!)
            Diagnose.result.append(true)
            Diagnose.finalReportResult.append(Result(name: "3DTouch".localized, is_worked: true, image: UIImage(named: "3dtouch"), data: nil))
            Diagnose.GroupEPrograss?.value += 5
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
        
    }
    
    
    @objc func buttonCancelClicked(){
        if self.workItem.isCancelled {
            return
        }
        self.myView?.removeFromSuperview()
        Diagnose.finalReportResult.append(Result(name: "3DTouch".localized, is_worked: false, image: UIImage(named: "3dtouch"), data: nil))
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
            self.Label.addError(view: self.viewC!)
            Diagnose.GroupEPrograss?.value += 5
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
        
    }
    
}


