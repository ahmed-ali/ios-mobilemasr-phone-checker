//
//  Touch.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import Foundation
import UIKit

class Touch {
    
    private init() {
        
    }
    
    static let instance = Touch()
    
    var application: UIApplication?
    var collectionViewGest: UICollectionView?
    var timesCount = 0
    var viewC: UIViewController?
    var Label : UILabel!
    
    var completion: (() -> Void)!
//    var myView : UIView?
    let buttonCancel:UIButton = UIButton(frame: CGRect(x: 40, y: 100, width: 20, height: 20))
    var workItem: DispatchWorkItem!
    func checkTouch(view:UIViewController, mainlabel:UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            mainlabel.addRunning(view: view)
            
            self.viewC = view
            self.completion = completion
            self.Label = mainlabel
            // create the alert
            if self.workItem.isCancelled {
                return
            }
            let alert = UIAlertController(title: "TestMultiple-Touch".localized, message: "MultiTouchMessage".localized, preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: { action in
                if self.workItem.isCancelled {
                    return
                }
                let myView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width
                                                  , height: UIScreen.main.bounds.height
                ))
                Diagnose.MultitouchView = myView
                myView.backgroundColor = .white
                view.view.addSubview(myView)
                self.buttonCancel.backgroundColor = .clear
                self.buttonCancel.setImage(UIImage(named: "cancel"), for: .normal)
                self.buttonCancel.addTarget(self, action:#selector(self.buttonCancelClicked), for: .touchUpInside)
                myView.addSubview(self.buttonCancel)
                let view1 = UIView(frame: CGRect(x: UIScreen.main.bounds.width - 150, y: 150, width: 100, height: 100))
                view1.setBorderUIView(color: #colorLiteral(red: 0, green: 0.3411764706, blue: 0.4117647059, alpha: 1), borderWidth: 1, cornerRadius: 50)
                myView.addSubview(view1)
                let view2 = UIView(frame: CGRect(x: 100, y: UIScreen.main.bounds.height - 200, width: 100, height: 100))
                view2.setBorderUIView(color: #colorLiteral(red: 0, green: 0.3411764706, blue: 0.4117647059, alpha: 1), borderWidth: 1, cornerRadius: 50)
                myView.addSubview(view2)
                
                
                Diagnose.MultiapleTouch = true
                if self.workItem.isCancelled {
                    return
                }
                
            }))
            
            // show the alert
            view.present(alert, animated: true, completion: nil)
        }
        
        DispatchQueue.main.async(execute: self.workItem)
        
    }
    
    
    @objc func buttonCancelClicked(){
        if self.workItem.isCancelled {
            return
        }
        Diagnose.MultiapleTouch = false
        Diagnose.MultitouchView?.removeFromSuperview()
        Diagnose.finalReportResult.append(Result(name: "MultipleTouch".localized, is_worked: false, image: UIImage(named: "MultiTouch"), data: nil))
        if self.workItem.isCancelled {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
            self.Label.addError(view: self.viewC!)
            Diagnose.GroupEPrograss?.value += 5
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
        
    }
    func actionError(){
        if self.workItem.isCancelled {
            return
        }
        Diagnose.MultitouchView?.removeFromSuperview()
        Diagnose.MultiapleTouch = false
        Diagnose.finalReportResult.append(Result(name: "MultipleTouch".localized, is_worked: false, image: UIImage(named: "MultiTouch"), data: nil))
        if self.workItem.isCancelled {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
           
            self.Label.addError(view: self.viewC!)
            Diagnose.GroupEPrograss?.value += 5
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
    }
    
    func actionSuccess() {
        if self.workItem.isCancelled {
        return
    }
        Diagnose.MultitouchView?.removeFromSuperview()
        Diagnose.MultiapleTouch = false
        Diagnose.finalReportResult.append(Result(name: "MultipleTouch".localized, is_worked: true, image: UIImage(named: "MultiTouch"), data: nil))
        if self.workItem.isCancelled {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
            self.Label.addSuccess(view: self.viewC!)
            Diagnose.result.append(true)
            Diagnose.GroupEPrograss?.value += 5
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
    }
}


extension GroupEVC {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if Diagnose.MultiapleTouch == true {
//            print(event?.allTouches?.count)
            if event?.allTouches?.count ?? 0 > 1{
                print("hello")
                Touch.instance.actionSuccess()
            }

        }
        
    }
}
