//
//  VolumeDownBTN.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import Foundation
import UIKit
import MediaPlayer

class VolumeDown {
    
    private init() {
//        NotificationCenter.default.addObserver(self, selector: #selector(self.volumeChanged(_:)), name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    static let instance = VolumeDown()
    
    let  audioSession = AVAudioSession.sharedInstance()
    var viewC:UIViewController!
    var label : UILabel!
    var completion: (() -> Void)!
    var volume2:Float! = 1
    var timesCount = 0
    var workItem: DispatchWorkItem!
    func checkVolumeDown(view:UIViewController, label:UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.async {
                self.viewC = view
                self.label = label
                self.completion = completion
                do{
                 try self.audioSession.setActive(true)
                 self.volume2 = self.audioSession.outputVolume
                    //                 print(self.volume2?.description) //gets initial volume
                  }
                catch{
                    print("Error info: \(error)")
                }
                NotificationCenter.default.addObserver(self, selector: #selector(self.volumeChanged(_:)), name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
                
                label.addRunning(view: view)
                print(self.audioSession.outputVolume)
                if self.workItem.isCancelled {
                    return
                }
                let alert = UIAlertController(title: "TestVolumeDownButton".localized, message: "VolumeDownMessage".localized, preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: {_ in
                    if self.workItem.isCancelled {
                        return
                    }
                    NotificationCenter.default.removeObserver(self)
                    Diagnose.finalReportResult.append(Result(name: "VolumeDownButton".localized, is_worked: false, image: UIImage(named: "volume-down"), data: nil))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.GroupDPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                }))
                
                // show the alert
                view.present(alert, animated: true, completion: nil)
            }
            
            
            
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
    
    @objc func volumeChanged(_ notification: NSNotification) {
        if self.workItem.isCancelled {
            return
        }
        if let volume = notification.userInfo!["AVSystemController_AudioVolumeNotificationParameter"] as? Float {
            print("volume: \(volume)")
            
            if (volume < volume2 || volume == 0){
                if self.workItem.isCancelled {
                    return
                }
                
                var isAlertViewPresenting: Bool {
                    get {
                        if self.viewC.presentedViewController is UIAlertController {
                            return true
                        }
                        return false
                    }
                }
                if isAlertViewPresenting{
                    self.viewC.dismiss(animated: true, completion: nil)
                }
                NotificationCenter.default.removeObserver(self)
                Diagnose.finalReportResult.append(Result(name: "VolumeDownButton".localized, is_worked: true, image: UIImage(named: "volume-down"), data: nil))
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    self.label.addSuccess(view: self.viewC)
                    Diagnose.result.append(true)
                    Diagnose.GroupDPrograss?.value += 3
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.completion()
                    }
                }
            }
            else{
                if self.workItem.isCancelled {
                    return
                }
                var isAlertViewPresenting: Bool {
                    get {
                        if self.viewC.presentedViewController is UIAlertController {
                            return true
                        }
                        return false
                    }
                }
                if isAlertViewPresenting{
                    self.viewC.dismiss(animated: true, completion: nil)
                }
                if self.workItem.isCancelled {
                    return
                }
                self.volume2 = volume
                if self.timesCount < 2 {
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1){
                        if self.workItem.isCancelled {
                            return
                        }
                        let alert = UIAlertController(title: "TestVolumeDownButton".localized, message: "VolumeDownMessage2".localized, preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        
                        alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: {_ in
                            if self.workItem.isCancelled {
                                return
                            }
                            NotificationCenter.default.removeObserver(self)
                            Diagnose.finalReportResult.append(Result(name: "VolumeDownButton".localized, is_worked: false, image: UIImage(named: "volume-down"), data: nil))
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                self.label.addError(view: self.viewC)
                                Diagnose.GroupDPrograss?.value += 3
                                if self.workItem.isCancelled {
                                    return
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    if self.workItem.isCancelled {
                                        return
                                    }
                                    self.completion()
                                }
                            }
                        }))
                        
                        // show the alert
                        self.viewC.present(alert, animated: true, completion: nil)
                    }
                    self.timesCount += 1

                }
                else{
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.async {
                        if self.workItem.isCancelled {
                            return
                        }
                        NotificationCenter.default.removeObserver(self)
                        Diagnose.finalReportResult.append(Result(name: "VolumeDownButton".localized, is_worked: false, image: UIImage(named: "volume-down"), data: nil))
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            self.label.addError(view: self.viewC)
                            Diagnose.GroupDPrograss?.value += 3
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                self.completion()
                            }
                        }
                    }
                }
                
                
            }
        }
        
        
    }
}


