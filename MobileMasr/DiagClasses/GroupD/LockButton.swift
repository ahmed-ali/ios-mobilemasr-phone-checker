//
//  LockButton.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import Foundation
import UIKit

class LockButton {
    private init() {
        
    }
    deinit {
    }
    
    
    static let instance = LockButton()
    
    
    var viewC:UIViewController!
    var label : UILabel!
    var completion: (() -> Void)!
    var workItem: DispatchWorkItem!
    func checkLockButton(view:UIViewController, label:UILabel,completion: @escaping ()->Void) {
        UIApplication.shared.isIdleTimerDisabled = true

        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                UIApplication.shared.isIdleTimerDisabled = false

                return
            }
            UIApplication.shared.isIdleTimerDisabled = true
            label.addRunning(view: view)
            Diagnose.lockButton = true

            self.viewC = view
            self.label = label
            self.completion = completion
            // create the alert
            
            let alert = UIAlertController(title: "TestlockButton".localized, message: "LockButtonMessage".localized, preferredStyle: UIAlertController.Style.alert)
            if self.workItem.isCancelled {
                UIApplication.shared.isIdleTimerDisabled = false
                return
            }
            // add an action (button)
            alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: {_ in
                if self.workItem.isCancelled {
                    UIApplication.shared.isIdleTimerDisabled = false
                    return
                }
                Diagnose.finalReportResult.append(Result(name: "LockButton".localized, is_worked: false, image: UIImage(named: "unlock"), data: nil))
                if self.workItem.isCancelled {
                    UIApplication.shared.isIdleTimerDisabled = false
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        UIApplication.shared.isIdleTimerDisabled = false
                        return
                    }
                    label.addError(view: view)
                    Diagnose.GroupDPrograss?.value += 3
                    Diagnose.lockButton = false
                    if self.workItem.isCancelled {
                        UIApplication.shared.isIdleTimerDisabled = false
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            UIApplication.shared.isIdleTimerDisabled = false
                            return
                        }
                        completion()
                    }
                }
            }))
            
            // show the alert
            view.present(alert, animated: true, completion: nil)
        }
        DispatchQueue.main.async(execute: self.workItem)
        
    }
    
    func checkDone() {
        UIApplication.shared.isIdleTimerDisabled = false

        if self.workItem.isCancelled {
            UIApplication.shared.isIdleTimerDisabled = false

            return
        }
        var isAlertViewPresenting: Bool {
            get {
                if self.viewC.presentedViewController is UIAlertController {
                    return true
                }
                return false
            }
        }
        if isAlertViewPresenting{
            self.viewC.dismiss(animated: true, completion: nil)
        }
        Diagnose.lockButton = false
        Diagnose.finalReportResult.append(Result(name: "LockButton".localized, is_worked: true, image: UIImage(named: "unlock"), data: nil))
        if self.workItem.isCancelled {
            UIApplication.shared.isIdleTimerDisabled = false

            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                UIApplication.shared.isIdleTimerDisabled = false
                return
            }
            self.label.addSuccess(view: self.viewC)
            Diagnose.result.append(true)
            Diagnose.GroupDPrograss?.value += 3
            if self.workItem.isCancelled {
                UIApplication.shared.isIdleTimerDisabled = false
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    UIApplication.shared.isIdleTimerDisabled = false
                return
            }
                self.completion()
            }
        }
    }
    
    
    
}
