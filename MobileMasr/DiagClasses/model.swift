//
//  model.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import Foundation
import UIKit
import MBCircularProgressBar

struct Diagnose {
    static var action : [NSMutableAttributedString] = []
   
    static var result : [Bool] = []
    static var resultScorePoint : [CGFloat] = []
    static var camera : String = "back"
    static var lockButton : Bool = false
    static var collection : UICollectionView?
    static var touchboxesColor: [Bool] = []
    static var continuousboxesColor: [Bool] = []
    static var touchView : UIView?
    static var continuoustouchView : UIView?
    static var MultitouchView : UIView?
    static var touchCount = 0
    static var continuousTouchCount = 0
    static var MultiapleTouch : Bool = false
    static var prograssCount = 0
    static var GroupAPrograss: MBCircularProgressBarView?
    static var GroupBPrograss: MBCircularProgressBarView?
    static var GroupCPrograss: MBCircularProgressBarView?
    static var GroupDPrograss: MBCircularProgressBarView?
    static var GroupEPrograss: MBCircularProgressBarView?
    static var videoController: UIImagePickerController?
    static var timer : Timer?
    static var finalResultPrograss : CGFloat  = 0
    static var funcContTouch : (() -> Void)!
    static var finalReportResult = [Result]()
    

    static let imageAttachment = UIImageView()
    

}

struct Result {
    let name: String?
    let is_worked: Bool?
    let image: UIImage?
    let data: [DataResult]?
    
}

struct DataResult {
    let name: String?
    let desc: String?
    
    
}
