//
//  GroupEVCextenion.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import Foundation
import UIKit
//import GoogleMobileVision
import MLKit
import AVFoundation


extension GroupEVC: UIImagePickerControllerDelegate & UINavigationControllerDelegate{
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        if Diagnose.camera == "back" {
            BackCamer.instance.actionError()
        }
        else{
            FrontCamer.instance.actionError()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        
        guard let image = info[.editedImage] as? UIImage else {
            print("No image found")
            return
        }
        if Diagnose.camera == "back" {
            
            
            
            //
            let options = FaceDetectorOptions()
            options.performanceMode = .accurate
            options.landmarkMode = .all
            options.classificationMode = .all

            // Real-time contour detection of multiple faces
             options.contourMode = .all
            let visionImage : VisionImage? = nil
            let image2 = VisionImage(image: image)
            visionImage?.orientation = image2.orientation
            
            let textRecognizer = TextRecognizer.textRecognizer()
            textRecognizer.process(image2) { result, error in
                if error != nil || result?.text == "" || result?.text == nil{
                    let faceDetector = FaceDetector.faceDetector(options: options)
                    weak var weakSelf = self
        //            faceDetector.process(<#T##image: MLKitCompatibleImage##MLKitCompatibleImage#>, completion: <#T##FaceDetectionCallback##FaceDetectionCallback##([Face]?, Error?) -> Void#>)
                    faceDetector.process(image2) { faces, error in
                        guard weakSelf != nil else {
                            Diagnose.videoController?.stopVideoCapture()
                            let alert = UIAlertController(title: "TestBackCamera".localized, message: "BackCameraMessage2".localized, preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            if self.timesCount < 2 {
                                alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {_ in
                                    BackCamer.instance.checkCamera(view: self, label: self.rearCamLBL) {
                                        FrontCamer.instance.checkFrontCamer(view: self, label: self.frontCamLBL) {
                                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                            let vc = storyboard.instantiateViewController(withIdentifier: "FinalResultVC") as! FinalResultVC
                                            self.navigationController?.pushViewController(vc,
                                                                                          animated: true)
                                        }
                                    }
                                }))
                                self.timesCount += 1
                            }
                            
                            alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: {_ in
                                
                                
                                BackCamer.instance.actionError()
                                
                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                        return
                      }
                      guard error == nil, let faces = faces, !faces.isEmpty else {
        //                BackCamer.instance.actionError()
                        Diagnose.videoController?.stopVideoCapture()
                    let alert = UIAlertController(title: "TestBackCamera".localized, message: "BackCameraMessage2".localized, preferredStyle: UIAlertController.Style.alert)
                    // add an action (button)
                    if self.timesCount < 2 {
                        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {_ in
                            BackCamer.instance.checkCamera(view: self, label: self.rearCamLBL) {
                                FrontCamer.instance.checkFrontCamer(view: self, label: self.frontCamLBL) {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "FinalResultVC") as! FinalResultVC
                                    self.navigationController?.pushViewController(vc,animated: true)
                                }
                            }
                        }))
                        self.timesCount += 1
                    }
                    alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: {_ in
                        BackCamer.instance.actionError()
                    }))
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    return
                        
                      }
                      // Faces detected
                        BackCamer.instance.actionSuccess2()
                    }
                    
              }
              else{
                let resultText = result?.text
//                          print("Hello",resultText)
                BackCamer.instance.actionSuccess(text: resultText ?? "no text")
              }
            }
            /////////////////////////////////////////
            
            
        }
        else{
            
            
            //
            let options = FaceDetectorOptions()
            options.performanceMode = .accurate
            options.landmarkMode = .all
            options.classificationMode = .all

            // Real-time contour detection of multiple faces
             options.contourMode = .all
            let visionImage : VisionImage? = nil
            let image2 = VisionImage(image: image)
            visionImage?.orientation = image2.orientation
            
            let faceDetector = FaceDetector.faceDetector(options: options)
            weak var weakSelf = self
//            faceDetector.process(<#T##image: MLKitCompatibleImage##MLKitCompatibleImage#>, completion: <#T##FaceDetectionCallback##FaceDetectionCallback##([Face]?, Error?) -> Void#>)
            faceDetector.process(image2) { faces, error in
                guard weakSelf != nil else {
                    let alert = UIAlertController(title: "TestFrontCamera".localized, message: "BackCameraMessage2".localized, preferredStyle: UIAlertController.Style.alert)
                    
                    // add an action (button)
                    if self.timesCount < 2 {
                        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {_ in
                            
                                FrontCamer.instance.checkFrontCamer(view: self, label: self.frontCamLBL) {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "FinalResultVC") as! FinalResultVC
                                    self.navigationController?.pushViewController(vc,
                                                                                  animated: true)
                                }
                            
                        }))
                        self.timesCount += 1
                    }
                    
                    alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: {_ in
                        
                        
                        FrontCamer.instance.actionError()
                        
                    }))
                    
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                return
              }
              guard error == nil, let faces = faces, !faces.isEmpty else {
                
                let textRecognizer = TextRecognizer.textRecognizer()
                textRecognizer.process(image2) { result, error in
                    if error != nil || result?.text == "" || result?.text == nil{
                    
                    let alert = UIAlertController(title: "TestFrontCamera".localized, message: "BackCameraMessage2".localized, preferredStyle: UIAlertController.Style.alert)
                    // add an action (button)
                    if self.timesCount < 2 {
                        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {_ in
                            
                                FrontCamer.instance.checkFrontCamer(view: self, label: self.frontCamLBL) {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let vc = storyboard.instantiateViewController(withIdentifier: "FinalResultVC") as! FinalResultVC
                                    self.navigationController?.pushViewController(vc,animated: true)
                                }
                            
                        }))
                        self.timesCount += 1
                    }

                    alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: {_ in
                        FrontCamer.instance.actionError()
                    }))
                    // show the alert
                    self.present(alert, animated: true, completion: nil)
                    return
                  }
                  else{
                    
                        // Recognized text
                    let resultText = result?.text
                          
//                          print("Hello",resultText)
                    FrontCamer.instance.actionSuccess(text: resultText ?? "no text")
                    

//                        let alert = UIAlertController(title: "TestBackCamera".localized, message: "BackCameraMessage2".localized, preferredStyle: UIAlertController.Style.alert)
//                        // add an action (button)
//                        if self.timesCount < 2 {
//                            alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {_ in
//                                BackCamer.instance.checkCamera(view: self, label: self.PhoneVLBL) {
//                                    FrontCamer.instance.checkFrontCamer(view: self, label: self.iOSVLBL) {
//                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                                        let vc = storyboard.instantiateViewController(withIdentifier: "FinalResultVC") as! FinalResultVC
//                                        self.navigationController?.pushViewController(vc,animated: true)
//                                    }
//                                }
//                            }))
//                            self.timesCount += 1
//                        }
//
//                        alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: {_ in
//                            BackCamer.instance.actionError()
//                        }))
//                        // show the alert
//                        self.present(alert, animated: true, completion: nil)
//                        return

                    
                    
                  }
                  
                }
//                BackCamer.instance.actionError()
                return
              }

              // Faces detected
                FrontCamer.instance.actionSuccess2()
   
            }
        
        }
        
    }
    
    func imageOrientation(
      deviceOrientation: UIDeviceOrientation,
      cameraPosition: AVCaptureDevice.Position
    ) -> UIImage.Orientation {
      switch deviceOrientation {
      case .portrait:
        return cameraPosition == .front ? .leftMirrored : .right
      case .landscapeLeft:
        return cameraPosition == .front ? .downMirrored : .up
      case .portraitUpsideDown:
        return cameraPosition == .front ? .rightMirrored : .left
      case .landscapeRight:
        return cameraPosition == .front ? .upMirrored : .down
      case .faceDown, .faceUp, .unknown:
        return .up
      @unknown default:
        return .upMirrored
      }
    }
}


