//
//  ID.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import Foundation
import UIKit
import LocalAuthentication

class ID {
    
    private init() {}
    static let instance = ID()
    let context = LAContext()
    var error: NSError?
    var workItem: DispatchWorkItem!
    
    func checkID(view:UIViewController, label:UILabel,completion: @escaping ()->Void)  {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            label.addRunning(view: view)
            if self.context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &self.error) {
                let reason = "Test ID"

                self.context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                     success, authenticationError in
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.async {
                        if success {
                            if self.workItem.isCancelled {
                                return
                            }
                            Diagnose.finalReportResult.append(Result(name: "ID".localized, is_worked: true, image: UIImage(named: "fingerprint"), data: nil))
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                label.addSuccess(view: view)
                                Diagnose.result.append(true)
                                Diagnose.GroupCPrograss?.value += 3
                                if self.workItem.isCancelled {
                                    return
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    if self.workItem.isCancelled {
                                        return
                                    }
                                    completion()
                                }
                            }
                        } else {
                            if self.workItem.isCancelled {
                                return
                            }
                            Diagnose.finalReportResult.append(Result(name: "ID".localized, is_worked: false, image: UIImage(named: "fingerprint"), data: nil))
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                label.addError(view: view)
                                Diagnose.GroupCPrograss?.value += 3
                                if self.workItem.isCancelled {
                                    return
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    if self.workItem.isCancelled {
                                        return
                                    }
                                    completion()
                                }
                            }
                        }
                    }
                }
            } else {
                if self.workItem.isCancelled {
                    return
                }
                Diagnose.finalReportResult.append(Result(name: "ID".localized, is_worked: false, image: UIImage(named: "fingerprint"), data: nil))
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addError(view: view)
                    Diagnose.GroupCPrograss?.value += 3
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            }
        }
        DispatchQueue.main.async(execute: self.workItem)
        
    }
    
}
