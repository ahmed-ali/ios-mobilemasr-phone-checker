//
//  FrontCamera.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 17/06/2021.
//

import Foundation
import CoreMotion
//import GoogleMobileVision
import UIKit

class FrontCamer {
    var workItem: DispatchWorkItem!
    private init() {}
    static let instance = FrontCamer()
    var viewC:UIViewController!
    var label : UILabel!
    var completion: (() -> Void)!
    
    func checkFrontCamer(view:UIViewController, label:UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            label.addRunning(view: view)
            self.viewC = view
            self.label = label
            self.completion = completion
            if self.workItem.isCancelled {
                return
            }
            let alert = UIAlertController(title: "TestFrontCamera".localized, message: "BackCameraMessage".localized, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
            alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: {_ in
                if self.workItem.isCancelled {
                    return
                }
                        let vc = UIImagePickerController()
                        vc.sourceType = .camera
                        vc.allowsEditing = true
                        vc.cameraDevice = .front
                        vc.delegate = view as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
                        view.present(vc, animated: true)
                if self.workItem.isCancelled {
                    return
                }
                    }))
            alert.view.layoutIfNeeded()
                    // show the alert
                    view.present(alert, animated: true, completion: nil)
            
        }
        
        DispatchQueue.main.async(execute: self.workItem)
    }
    
    func actionError(){
        if self.workItem.isCancelled {
            return
        }
        Diagnose.finalReportResult.append(Result(name: "FrontCamera".localized, is_worked: false, image: UIImage(named: "front-camera"), data: nil))
        if self.workItem.isCancelled {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
            Diagnose.camera = "front"
            self.label.addError(view: self.viewC)
            Diagnose.GroupEPrograss?.value += 8
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
    }
    
    func actionSuccess(text:String) {
        
        if self.workItem.isCancelled {
            return
        }
        Diagnose.finalReportResult.append(Result(name: "FrontCamera".localized, is_worked: true, image: UIImage(named: "front-camera"), data: [DataResult(name: "Textrecognized".localized, desc: text)]))
        if self.workItem.isCancelled {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
            Diagnose.camera = "front"
            self.label.addSuccess(view: self.viewC)
            Diagnose.result.append(true)
            Diagnose.GroupEPrograss?.value += 8
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
    }
    
    func actionSuccess2() {
        if self.workItem.isCancelled {
            return
        }
        
        Diagnose.finalReportResult.append(Result(name: "FrontCamera".localized, is_worked: true, image: UIImage(named: "front-camera"), data: nil))
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
            Diagnose.camera = "front"
            self.label.addSuccess(view: self.viewC)
            Diagnose.result.append(true)
            Diagnose.GroupEPrograss?.value += 8
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
    }
}


