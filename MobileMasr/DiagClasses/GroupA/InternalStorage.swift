//
//  InternalStorage.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit


class Storage {
    private init() {}
    static let instance = Storage()
    var workItem: DispatchWorkItem!
    func checkStorage(view:UIViewController, label:UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.async {
                label.addRunning(view: view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    //                get  disk data by extention function
                    Diagnose.result.append(true)
                    Diagnose.GroupAPrograss?.value += 3
                    label.addText(view: view, text:"Used".localized +  "\(UIDevice.current.usedDiskSpaceInGB)" +  "Of".localized + " \(UIDevice.current.totalDiskSpaceInGB)"  )
                    print("Used".localized +  "\(UIDevice.current.usedDiskSpaceInGB)" +  "Of".localized + " \(UIDevice.current.totalDiskSpaceInGB)")
                    Diagnose.finalReportResult.append(Result(name: "InternalStorage".localized, is_worked: true, image: UIImage(named: "server-storage"), data: [DataResult(name: "StorageUsage".localized, desc: "\(UIDevice.current.usedDiskSpaceInGB)" + "Of".localized +  "\(UIDevice.current.totalDiskSpaceInGB)" + "Used".localized)]))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            }
        }
        
        DispatchQueue.main.async(execute: self.workItem)
        
    }
    
    func cancel() {
        self.workItem.cancel()
    }
    
}
