//
//  Thermal.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit


class Thermal {
    private init() {}
    static let instance = Thermal()
    var workItem: DispatchWorkItem!
    func checkThermal(view:UIViewController, label:UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.async {
                label.addRunning(view: view)
            }
            
            
            switch ProcessInfo.processInfo.thermalState {
    //       normal temprature
            case .nominal:
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addSuccess(view: view)
                    Diagnose.GroupCPrograss?.value += 3
                    Diagnose.result.append(true)
                    Diagnose.finalReportResult.append(Result(name: "Thermal".localized, is_worked: true, image: UIImage(named: "thermal"), data: [DataResult(name: "CurrentThermal".localized, desc: "Normal".localized)]))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            //       fair temprature
            case .fair:
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addError(view: view)
                    Diagnose.GroupCPrograss?.value += 3
                    Diagnose.finalReportResult.append(Result(name: "Thermal".localized, is_worked: false, image: UIImage(named: "thermal"), data: [DataResult(name: "CurrentThermal".localized, desc: "Slightlyelevated".localized)]))
                    if self.workItem.isCancelled {
                        return
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            //       serious temprature
            case .serious:
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addError(view: view)
                    Diagnose.GroupCPrograss?.value += 3
                    Diagnose.finalReportResult.append(Result(name: "Thermal".localized, is_worked: false, image: UIImage(named: "thermal"), data: [DataResult(name: "CurrentThermal".localized, desc: "High".localized)]))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            //       critical temprature
            case .critical:
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addError(view: view)
                    Diagnose.GroupCPrograss?.value += 3
                    Diagnose.finalReportResult.append(Result(name: "Thermal".localized, is_worked: false, image: UIImage(named: "thermal"), data: [DataResult(name: "CurrentThermal".localized, desc: "Toohigh".localized)]))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
    //            when can not check
            @unknown default:
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addError(view: view)
                    Diagnose.GroupCPrograss?.value += 3
                    Diagnose.finalReportResult.append(Result(name: "Thermal".localized, is_worked: false, image: UIImage(named: "thermal"), data: [DataResult(name: "CurrentThermal".localized, desc: "Undefined".localized)]))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            }
        }
        
        DispatchQueue.main.async(execute: self.workItem)
}
}
