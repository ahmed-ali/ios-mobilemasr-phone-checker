//
//  Memory.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit



class Memory {
    private init() {}
    static let instance = Memory()
    var workItem: DispatchWorkItem!
    func checkMemory(view:UIViewController, label:UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            DispatchQueue.main.async {
                if self.workItem.isCancelled {
                    return
                }
                label.addRunning(view: view)
                UIDevice.current.isBatteryMonitoringEnabled = true
    //            get memory size
                var size = Float(ProcessInfo.processInfo.physicalMemory) / Float(1024.0 * 1024.0 * 1024.0)
                size = Float(Double(size).rounded(toPlaces: 1))
                let usedDisk = String(size)
                
                Diagnose.GroupCPrograss?.value += 3
                Diagnose.result.append(true)
                Diagnose.finalReportResult.append(Result(name: "Memory".localized, is_worked: true, image: UIImage(named: "ram"), data: [DataResult(name: "CurrentRam".localized, desc: "Avilable".localized + " ≈ "  + usedDisk + "GB".localized)]))
                
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addText(view: view, text: "Avilable".localized + " ≈ "  + usedDisk + "GB".localized)
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        completion()
                    }
                }
            }
        }
        DispatchQueue.main.async(execute: self.workItem)

    }
    
}
extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
