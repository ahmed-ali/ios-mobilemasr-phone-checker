//
//  PhoneVersion.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit

class PhoneVersion {
    private init() {}
    static let instance = PhoneVersion()
    var workItem: DispatchWorkItem!
    func phoneVersion( view:UIViewController, label: UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.async {
                label.addRunning(view: view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    //               get phone version by extention function
                    Diagnose.result.append(true)
                    Diagnose.GroupAPrograss?.value += 3
                    label.addText(view: view, text: UIDevice.modelName)
                    print(UIDevice.modelName)
                    Diagnose.finalReportResult.append(Result(name: "PhoneVersion".localized, is_worked: true, image: UIImage(named: "mobile-phone-popular-model-apple-iphone-5s"), data: [DataResult(name: "Version".localized, desc: UIDevice.modelName)]))
                    if self.workItem.isCancelled {
                        return
                    }
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            }
        }
        
        DispatchQueue.main.async(execute: self.workItem)
        
    }
    
    
    func cancel() {
        self.workItem.cancel()
    }
}
