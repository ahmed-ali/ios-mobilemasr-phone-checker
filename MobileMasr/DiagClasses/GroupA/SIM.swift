//
//  SIM.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit
import CoreTelephony


class SIM {
    private init() {}
    static let instance = SIM()
    var workItem: DispatchWorkItem!
//    sim var true or false
    var availableSIM: Bool {
        return ((CTTelephonyNetworkInfo().serviceSubscriberCellularProviders?.first?.value.mobileNetworkCode) != nil)
    }
    
    func checkSimCard(view:UIViewController, label:UILabel,completion: @escaping ()->Void){
        self.workItem = DispatchWorkItem{
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.async {
                label.addRunning(view: view)
            }
            //    sim if true
            if (self.availableSIM == true){
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addSuccess(view: view)
                    Diagnose.result.append(true)
                    Diagnose.GroupBPrograss?.value += 3
                    
                    Diagnose.finalReportResult.append(Result(name: "SIM".localized, is_worked: true, image: UIImage(named: "sim-card"), data: nil))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            }
            //    sim if false
            else{
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addError(view: view)
                    Diagnose.GroupBPrograss?.value += 3
                    Diagnose.finalReportResult.append(Result(name: "SIM".localized, is_worked: false, image: UIImage(named: "sim-card"), data: nil))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            }
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
}
