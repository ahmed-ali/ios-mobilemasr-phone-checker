//
//  iOSVersion.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit

class IOSVersion {
    private init() {}
    static let instance = IOSVersion()
    var workItem: DispatchWorkItem!
    func iOSVersion( view:UIViewController, label: UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.async {
                label.addRunning(view: view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    //                get ios version
                    Diagnose.result.append(true)
                    Diagnose.GroupAPrograss?.value += 3
                    print(UIDevice.current.systemVersion)
                    label.addText(view: view, text: UIDevice.current.systemVersion)
                    Diagnose.finalReportResult.append(Result(name: "iOSVersion".localized, is_worked: true, image: UIImage(named: "apple"), data: [DataResult(name: "Version".localized, desc: UIDevice.current.systemVersion)]))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }
            }
        }
        
        DispatchQueue.main.async(execute: self.workItem)
        
    }
    
    func cancel() {
        self.workItem.cancel()
    }
}
