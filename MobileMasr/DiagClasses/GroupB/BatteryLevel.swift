//
//  BatteryLevel.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit
import AVFoundation

class BatteryLVL {
    private init() {}
    static let instance = BatteryLVL()
    var workItem: DispatchWorkItem!
    
    
    
    func checkBatteryLVL(view:UIViewController, label: UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            label.addRunning(view: view)
            UIDevice.current.isBatteryMonitoringEnabled = true
            // get battery level
            let level = String(UIDevice.current.batteryLevel * 100)
           
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                if self.workItem.isCancelled {
                    return
                }
                Diagnose.result.append(true)
                label.addText(view: view, text: level + "%")
                Diagnose.finalReportResult.append(Result(name: "BatteryLevel".localized, is_worked: true, image: UIImage(named: "batterylevel"), data: [DataResult(name: "Level".localized, desc: level + "%")]))
                if self.workItem.isCancelled {
                    return
                }
                Diagnose.GroupCPrograss?.value += 3
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    if self.workItem.isCancelled {
                        return
                    }
                    completion()
                }
            }
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
    
}
    
    

