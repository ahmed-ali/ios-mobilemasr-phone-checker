//
//  Bluetooth.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation

import CoreBluetooth
import UIKit


class BLEConnection: NSObject, CBCentralManagerDelegate, CBPeripheralDelegate {
    var centralManager: CBCentralManager!
    var myPeripheral: CBPeripheral!
    var viewC:UIViewController!
    var label : UILabel!
    var completion: (() -> Void)!
    static let instance = BLEConnection()
    var workItem: DispatchWorkItem!
    var timesCount = 0
    

func startCentralManager(view:UIViewController, label: UILabel,completion: @escaping ()->Void){
    
    self.workItem = DispatchWorkItem{
        if self.workItem.isCancelled {
            return
        }
        self.label = label
        DispatchQueue.main.async {
            label.addRunning(view: view)
        }
        self.viewC = view
        self.centralManager = CBCentralManager(delegate: self, queue: DispatchQueue.main,options: [CBCentralManagerOptionShowPowerAlertKey: false])
        
        self.completion = completion
        if self.workItem.isCancelled {
            return
        }
    }
    DispatchQueue.main.async(execute: self.workItem)
}

func centralManagerDidUpdateState(_ central: CBCentralManager) {
    if self.workItem.isCancelled {
        return
    }
    switch (central.state) {
    
       case .unsupported:
        if self.workItem.isCancelled {
            return
        }
        // create the alert
        DispatchQueue.main.async{
            if self.workItem.isCancelled {
                return
            }
            let alert = UIAlertController(title: "TestBluetooth".localized, message: "BluetoothMessage".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {action in
                if self.workItem.isCancelled {
                    return
                }
                    print("BLE is Unsupported")
                    self.centralManager = nil
                Diagnose.finalReportResult.append(Result(name: "Bluetooth".localized, is_worked: false, image: UIImage(named: "bluetooth"), data: nil))
                if self.workItem.isCancelled {
                    return
                }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.label.addError(view: self.viewC)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            self.completion()
                        }
                    }
                }))
                
            
            if self.timesCount < 2 {
                alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.default, handler: {action in
                    if self.workItem.isCancelled {
                        return
                    }
                    self.timesCount += 1
                    self.centralManagerDidUpdateState(central)
                }))
            }
            
                
        
                // show the alert
            self.viewC.present(alert, animated: true, completion: nil)
        }
        
        break
       case .unauthorized:
        print("BLE is Unauthorized")
        if self.workItem.isCancelled {
            return
        }
        // create the alert
        DispatchQueue.main.async{
            if self.workItem.isCancelled {
                return
            }
            let alert = UIAlertController(title: "TestBluetooth".localized, message: "BluetoothMessage2".localized, preferredStyle: UIAlertController.Style.alert)
                // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {action in
                if self.workItem.isCancelled {
                    return
                }
                    print("BLE is Unsupported")
                    self.centralManager = nil
                Diagnose.finalReportResult.append(Result(name: "Bluetooth".localized, is_worked: false, image: UIImage(named: "bluetooth"), data: nil))
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.label.addError(view: self.viewC)
                        Diagnose.GroupBPrograss?.value += 3
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            self.completion()
                        }
                    }
                }))
            if self.timesCount < 2 {
                alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.default, handler: {action in
                    if self.workItem.isCancelled {
                        return
                    }
                    self.timesCount += 1
                    self.centralManagerDidUpdateState(central)
                }))
            }
                // show the alert
            self.viewC.present(alert, animated: true, completion: nil)
        }
       
       case .unknown:
        if self.workItem.isCancelled {
            return
        }
        print("BLE is Unknown")
        // create the alert
        DispatchQueue.main.async{
            if self.workItem.isCancelled {
                return
            }
            let alert = UIAlertController(title: "TestBluetooth".localized, message: "BluetoothMessage".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {action in
                if self.workItem.isCancelled {
                    return
                }
                    print("BLE is Unsupported")
                    self.centralManager = nil
                Diagnose.finalReportResult.append(Result(name: "Bluetooth".localized, is_worked: false, image: UIImage(named: "bluetooth"), data: nil))
                if self.workItem.isCancelled {
                    return
                }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.label.addError(view: self.viewC)
                        Diagnose.GroupBPrograss?.value += 3
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            self.completion()
                        }
                    }
                }))
            if self.timesCount < 2 {
                alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.default, handler: {action in
                    if self.workItem.isCancelled {
                        return
                    }
                    self.timesCount += 1
                    self.centralManagerDidUpdateState(central)
                }))
            }
                // show the alert
            self.viewC.present(alert, animated: true, completion: nil)
        }
       case .resetting:
        if self.workItem.isCancelled {
            return
        }
        print("BLE is Resetting")
        // create the alert
        DispatchQueue.main.async{
            if self.workItem.isCancelled {
                return
            }
            let alert = UIAlertController(title: "TestBluetooth".localized, message: "BluetoothMessage".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {action in
                if self.workItem.isCancelled {
                    return
                }
                    print("BLE is Unsupported")
                    self.centralManager = nil
                Diagnose.finalReportResult.append(Result(name: "Bluetooth".localized, is_worked: false, image: UIImage(named: "bluetooth"), data: nil))
                if self.workItem.isCancelled {
                    return
                }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.label.addError(view: self.viewC)
                        Diagnose.GroupBPrograss?.value += 3
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            self.completion()
                        }
                    }
                }))
            if self.timesCount < 2 {
                alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.default, handler: {action in
                    self.timesCount += 1
                    self.centralManagerDidUpdateState(central)
                }))
            }
                // show the alert
            self.viewC.present(alert, animated: true, completion: nil)
        }
       case .poweredOff:
        if self.workItem.isCancelled {
            return
        }
        print("BLE is Powered Off")
        // create the alert
        DispatchQueue.main.async{
            if self.workItem.isCancelled {
                return
            }
            let alert = UIAlertController(title: "TestBluetooth".localized, message: "BluetoothMessage".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {action in
                if self.workItem.isCancelled {
                    return
                }
                    print("BLE is Unsupported")
                    self.centralManager = nil
                Diagnose.finalReportResult.append(Result(name: "Bluetooth".localized, is_worked: false, image: UIImage(named: "bluetooth"), data: nil))
                if self.workItem.isCancelled {
                    return
                }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.label.addError(view: self.viewC)
                        Diagnose.GroupBPrograss?.value += 3
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            self.completion()
                        }
                    }
                }))
            if self.timesCount < 2 {
                alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.default, handler: {action in
                    if self.workItem.isCancelled {
                        return
                    }
                    self.timesCount += 1
                    self.centralManagerDidUpdateState(central)
                }))
            }
                // show the alert
            self.viewC.present(alert, animated: true, completion: nil)
        }
       case .poweredOn:
        if self.workItem.isCancelled {
            return
        }
        print("BLE powered on")
        self.centralManager = nil
        Diagnose.result.append(true)
        Diagnose.finalReportResult.append(Result(name: "Bluetooth".localized, is_worked: true, image: UIImage(named: "bluetooth"), data: nil))
        if self.workItem.isCancelled {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
            self.label.addSuccess(view: self.viewC)
            Diagnose.GroupBPrograss?.value += 3
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
    @unknown default:
        if self.workItem.isCancelled {
            return
        }
        DispatchQueue.main.async{
            if self.workItem.isCancelled {
                return
            }
            let alert = UIAlertController(title: "TestBluetooth".localized, message: "BluetoothMessage".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {action in
                if self.workItem.isCancelled {
                    return
                }
                    print("BLE is Unsupported")
                    self.centralManager = nil
                Diagnose.finalReportResult.append(Result(name: "Bluetooth".localized, is_worked: false, image: UIImage(named: "bluetooth"), data: nil))
                if self.workItem.isCancelled {
                    return
                }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.label.addError(view: self.viewC)
                        Diagnose.GroupBPrograss?.value += 3
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            self.completion()
                        }
                    }
                }))
            if self.timesCount < 2 {
                alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.default, handler: {action in
                    if self.workItem.isCancelled {
                        return
                    }
                    self.timesCount += 1
                    self.centralManagerDidUpdateState(central)
                }))
            }
                // show the alert
            self.viewC.present(alert, animated: true, completion: nil)
        }
    }
}
   
}


