//
//  GPS.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import CoreLocation
import UIKit

class GPS {
    private init() {}
    static let instance = GPS()
    var workItem: DispatchWorkItem!
    func checkGPS(view:UIViewController,locationManager:CLLocationManager,label:UILabel,completion: @escaping ()->Void){
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            
    //        locationManager.requestLocation()
            label.addRunning(view: view)
            if self.workItem.isCancelled {
                return
            }
            if #available(iOS 14.0, *) {
                if self.workItem.isCancelled {
                    return
                }
                let status = locationManager.authorizationStatus
//                var currentLoc: CLLocation!
                // Handle each case of location permissions
                if self.workItem.isCancelled {
                    return
                }
                switch status {
                    case .authorizedAlways:
                        if self.workItem.isCancelled {
                            return
                        }
//                        currentLoc = locationManager.location
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            label.addSuccess(view: view)
                            Diagnose.GroupBPrograss?.value += 3
                            Diagnose.finalReportResult.append(Result(name: "GPS".localized, is_worked: true, image: UIImage(named: "gps"), data: nil))
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                completion()
                            }
                        }
                    case .authorizedWhenInUse:
                        if self.workItem.isCancelled {
                            return
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            label.addSuccess(view: view)
                            Diagnose.result.append(true)
                            Diagnose.GroupBPrograss?.value += 3
                            Diagnose.finalReportResult.append(Result(name: "GPS".localized, is_worked: true, image: UIImage(named: "gps"), data: nil))
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                completion()
                            }
                        }
                case .denied:
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.finalReportResult.append(Result(name: "GPS".localized, is_worked: false, image: UIImage(named: "gps"), data: nil))
                        Diagnose.GroupBPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                        // Handle case
                case .notDetermined:
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.finalReportResult.append(Result(name: "GPS".localized, is_worked: false, image: UIImage(named: "gps"), data: nil))
                        Diagnose.GroupBPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                        // Handle case
                case .restricted:
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.finalReportResult.append(Result(name: "GPS".localized, is_worked: false, image: UIImage(named: "gps"), data: nil))
                        Diagnose.GroupBPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                        // Handle case
                @unknown default:
    //                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
    //                    label.addError(view: view)
    //                    Diagnose.finalReportResult.append(Result(name: "GPS".localized, is_worked: false, image: UIImage(named: "gps"), data: nil))
    //                    Diagnose.GroupBPrograss?.value += 3
    //                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
    //                        completion()
    //                    }
    //                }
                break
                }
            }
            else{
                if self.workItem.isCancelled {
                    return
                }
                locationManager.requestWhenInUseAuthorization()
//                var currentLoc: CLLocation!
                if(CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
                CLLocationManager.authorizationStatus() == .authorizedAlways) {
//                   currentLoc = locationManager.location
                    Diagnose.finalReportResult.append(Result(name: "GPS".localized, is_worked: true, image: UIImage(named: "gps"), data: nil))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addSuccess(view: view)
                        Diagnose.result.append(true)
                        Diagnose.GroupBPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                }
                else{
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.finalReportResult.append(Result(name: "GPS".localized, is_worked: false, image: UIImage(named: "gps"), data: nil))
                        Diagnose.GroupBPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                }
            }
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
}
