//
//  CellularData.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit

class CheckCellularData {
    private init() {
    }
    deinit {
    }
    static let instance = CheckCellularData()
    var timesCount = 0
    var workItem: DispatchWorkItem!
    func checkCellularData(view:UIViewController, label: UILabel,completion: @escaping ()->Void){
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            label.addRunning(view: view)
            let networkStatus = Reachability().connectionStatus()
            
            switch networkStatus {
    //        wifi unknown
            case .Unknown:
                if self.workItem.isCancelled {
                    return
                }
                print("Unknown")
                DispatchQueue.main.async{
                    if self.workItem.isCancelled {
                        return
                    }
                    let alert = UIAlertController(title: "TestCellularData".localized, message: "CellularDataMessage".localized, preferredStyle: UIAlertController.Style.alert)

                        // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {action in
                        if self.workItem.isCancelled {
                            return
                        }
                            NotificationCenter.default.removeObserver(view, name: Notification.Name("ReachabilityStatusChangedNotification"), object: nil)
                        Diagnose.finalReportResult.append(Result(name: "CellularData".localized, is_worked: false, image: UIImage(named: "cellularData"), data: nil))
                        if self.workItem.isCancelled {
                            return
                        }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                label.addError(view: view)
                                Diagnose.GroupBPrograss?.value += 3
                                if self.workItem.isCancelled {
                                    return
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    if self.workItem.isCancelled {
                                        return
                                    }
                                    completion()
                                }
                            }
                        }))
                    if self.timesCount < 2 {
                        alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.default, handler: {action in
                            if self.workItem.isCancelled {
                                return
                            }
                            CheckCellularData.instance.checkCellularData(view: view,label: label){
                                completion()
                            }
                        }))
                        self.timesCount += 1
                    }
                        
                        // show the alert
                    view.present(alert, animated: true, completion: nil)
                }
                break
    //            offline
            case .Offline:
                if self.workItem.isCancelled {
                    return
                }
                print("Offline")
                DispatchQueue.main.async{
                    if self.workItem.isCancelled {
                        return
                    }
                    let alert = UIAlertController(title: "TestCellularData".localized, message: "CellularDataMessage".localized, preferredStyle: UIAlertController.Style.alert)

                    // add the actions (buttons)
                    alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {action in
                        if self.workItem.isCancelled {
                            return
                        }
                        NotificationCenter.default.removeObserver(view, name: Notification.Name("ReachabilityStatusChangedNotification"), object: nil)
                        Diagnose.finalReportResult.append(Result(name: "CellularData".localized, is_worked: false, image: UIImage(named: "cellularData"), data: nil))
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            label.addError(view: view)
                            Diagnose.GroupBPrograss?.value += 3
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                completion()
                            }
                        }
                    }))
                    if self.timesCount < 2 {
                        alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.default, handler: {action in
                            if self.workItem.isCancelled {
                                return
                            }
                            CheckCellularData.instance.checkCellularData(view: view,label: label){
                                completion()
                            }
                        }))
                        self.timesCount += 1
                    }
                    view.present(alert, animated: true, completion: nil)
                }
                break
    //            connected via Cellular Data
            case .Online(.WWAN):
                if self.workItem.isCancelled {
                    return
                }
                print("WWAN")
                DispatchQueue.main.async {
                    if self.workItem.isCancelled {
                        return
                    }
                    NotificationCenter.default.removeObserver(view, name: Notification.Name("ReachabilityStatusChangedNotification"), object: nil)
                    Diagnose.finalReportResult.append(Result(name: "CellularData".localized, is_worked: true, image: UIImage(named: "cellularData"), data: nil))
                    Diagnose.result.append(true)
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addSuccess(view: view)
                        Diagnose.GroupBPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                }
                
                
    //         connected via wifi
            case .Online(.WiFi):
                if self.workItem.isCancelled {
                    return
                }
                print("WiFi")
                let alert = UIAlertController(title: "TestCellularData".localized, message: "CellularDataMessage".localized, preferredStyle: UIAlertController.Style.alert)

                // add the actions (buttons)
                alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {action in
                    if self.workItem.isCancelled {
                        return
                    }
                    NotificationCenter.default.removeObserver(view, name: Notification.Name("ReachabilityStatusChangedNotification"), object: nil)
                    Diagnose.finalReportResult.append(Result(name: "CellularData".localized, is_worked: false, image: UIImage(named: "cellularData"), data: nil))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.GroupBPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                }))
                if self.timesCount < 2 {
                    alert.addAction(UIAlertAction(title: "TryAgain".localized, style: UIAlertAction.Style.default, handler: {action in
                        if self.workItem.isCancelled {
                            return
                        }
                        CheckCellularData.instance.checkCellularData(view: view,label: label){
                            completion()
                        }
                    }))
                    self.timesCount += 1
                }
                    view.present(alert, animated: true, completion: nil)
            }
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
}
