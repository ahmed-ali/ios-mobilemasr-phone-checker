//
//  BatteryState.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit
import AVFoundation

class BatteryState {
    private init() {}
    static let instance = BatteryState()
    var timesCount = 0
    var workItem: DispatchWorkItem!
    func checkBatteryState(view:UIViewController, label: UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem{
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.async {
                label.addRunning(view: view)
                UIDevice.current.isBatteryMonitoringEnabled = true
                if self.workItem.isCancelled {
                    return
                }
                switch UIDevice.current.batteryState {
                case .unknown:
                    if self.workItem.isCancelled {
                        return
                    }
                    let alert = UIAlertController(title: "TestBatteryState".localized, message: "BatteryMessage".localized, preferredStyle: UIAlertController.Style.alert)

                            // add an action (button)
                    if self.timesCount < 2 {
                        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: { action in
                            if self.workItem.isCancelled {
                                return
                            }
                                self.checkBatteryState(view:view, label: label) {
                                completion()
                            }
                        }))
                        self.timesCount += 1
                    }
                        
                    alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: { action in
                        if self.workItem.isCancelled {
                            return
                        }
                        Diagnose.finalReportResult.append(Result(name: "BatteryState".localized, is_worked: false, image: UIImage(named: "batterystate"), data: nil))
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            label.addError(view: view)
                            
                            Diagnose.GroupCPrograss?.value += 3
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                completion()
                            }
                        }
                    }))
                    // show the alert
                    view.present(alert, animated: true, completion: nil)
                case .unplugged:
                    if self.workItem.isCancelled {
                        return
                    }
                    let alert = UIAlertController(title: "TestBatteryState".localized, message: "BatteryMessage".localized, preferredStyle: UIAlertController.Style.alert)
                    // add an action (button)
                    
                    if self.timesCount < 2 {
                        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: { action in
                            if self.workItem.isCancelled {
                                return
                            }
                                self.checkBatteryState(view:view, label: label) {
                                completion()
                            }
                        }))
                        self.timesCount += 1
                    }
                    alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: { action in
                        if self.workItem.isCancelled {
                            return
                        }
                        Diagnose.finalReportResult.append(Result(name: "BatteryState".localized, is_worked: false, image: UIImage(named: "batterystate"), data: nil))
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            label.addError(view: view)
                            Diagnose.GroupCPrograss?.value += 3
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                completion()
                            }
                        }
                    }))
                    // show the alert
                    view.present(alert, animated: true, completion: nil)
                case .charging:
                    if self.workItem.isCancelled {
                        return
                    }
                    Diagnose.finalReportResult.append(Result(name: "BatteryState".localized, is_worked: true, image: UIImage(named: "batterystate"), data: nil))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addSuccess(view: view)
                        Diagnose.GroupCPrograss?.value += 3
                        Diagnose.result.append(true)
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                case .full:
                    if self.workItem.isCancelled {
                        return
                    }
                    Diagnose.finalReportResult.append(Result(name: "BatteryState".localized, is_worked: true, image: UIImage(named: "batterystate"), data: nil))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addSuccess(view: view)
                        Diagnose.result.append(true)
                        Diagnose.GroupCPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                @unknown default:
                    if self.workItem.isCancelled {
                        return
                    }
                    let alert = UIAlertController(title: "TestBatteryState".localized, message: "BatteryMessage".localized, preferredStyle: UIAlertController.Style.alert)
                    // add an action (button)
                    if self.timesCount < 2 {
                        alert.addAction(UIAlertAction(title: "OK".localized, style: UIAlertAction.Style.default, handler: { action in
                            if self.workItem.isCancelled {
                                return
                            }
                                self.checkBatteryState(view:view, label: label) {
                                completion()
                            }
                        }))
                        self.timesCount += 1
                    }
                    alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: { action in
                        if self.workItem.isCancelled {
                            return
                        }
                        Diagnose.finalReportResult.append(Result(name: "BatteryState".localized, is_worked: false, image: UIImage(named: "batterystate"), data: nil))
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            label.addError(view: view)
                            Diagnose.GroupCPrograss?.value += 3
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                completion()
                            }
                        }
                    }))
                    view.present(alert, animated: true, completion: nil)
                }
            }
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
}

