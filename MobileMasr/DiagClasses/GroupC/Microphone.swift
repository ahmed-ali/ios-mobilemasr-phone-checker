//
//  Microphone.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit
import Speech
import AVFoundation
import Pulsator




class Mic {
    private init() {}
    static let instance = Mic()
    var viewC:UIViewController!
    var mainLabel : UILabel!
    var completion: (() -> Void)!
    let myView = UIView(frame: CGRect(x: 25, y: UIScreen.main.bounds.height/5, width: UIScreen.main.bounds.width - 50, height: UIScreen.main.bounds.width - 50))
    let button:UIButton = UIButton(frame: CGRect(x: 100, y: 400, width: 75, height: 75))
    let buttonCancel:UIButton = UIButton(frame: CGRect(x: 20, y: 20, width: 20, height: 20))
    
    let label = UILabel()
    let pulsator = Pulsator()

    
    // Audio section
    var file:Int = 0
    var AudioPlayer:AVAudioPlayer!
    var AudioSession:AVAudioSession!
    var AudioRecorder:AVAudioRecorder!
    
    // Speech to Text section
    var lang: String = "en".localized
    var SpeechRecognitionTask: SFSpeechRecognitionTask?
    var RecognitionRequest: SFSpeechAudioBufferRecognitionRequest?
    var AudioEngine = AVAudioEngine()
    var SpeechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: "en".localized))
    var workItem: DispatchWorkItem!
    
    func checkMic(view:UIViewController, label: UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            self.viewC = view
            self.mainLabel = label
            label.addRunning(view: view)
            self.completion = completion
            
            
            // Initialize AudioSession
            self.AudioSession = AVAudioSession.sharedInstance()
            if self.workItem.isCancelled {
                return
            }
            // to solve 'required condition is false: IsFormatSampleRateAndChannelCountValid(format)'
            do {
                if self.workItem.isCancelled {
                    return
                }
                try AVAudioSession.sharedInstance().setCategory(.playAndRecord, mode: .default, options: [])
                try AVAudioSession.sharedInstance().setActive(true)
                if self.workItem.isCancelled {
                    return
                }
            } catch {
                print(error)
                if self.workItem.isCancelled {
                    return
                }
            }
            
            // To get permission from user
            AVAudioSession.sharedInstance().requestRecordPermission{(hasPermission) in
                if self.workItem.isCancelled {
                    return
                }
            }
            // To check any saved file?
            if let number:Int = UserDefaults.standard.object(forKey: "recording") as? Int
            {
                if self.workItem.isCancelled {
                return
            }
                self.file = number
            }
            
            
            
            
            if self.workItem.isCancelled {
                return
            }
            self.SpeechRecognizer?.delegate = self as? SFSpeechRecognizerDelegate
            self.SpeechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: self.lang))
            SFSpeechRecognizer.requestAuthorization { (authStatus) in
                
                if self.workItem.isCancelled {
                    return
                }
                
                switch authStatus {
                case .authorized:
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.async {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.myView.backgroundColor = .white
                        view.view.addSubview(self.myView)
                        self.button.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2117647059, blue: 0.3725490196, alpha: 1)
                        
                        self.button.center = CGPoint(x: self.myView.frame.size.width  / 2,
                                                     y: self.myView.frame.size.height / 2)
                        self.button.layer.cornerRadius = self.button.bounds.height / 2
                        self.button.setImage(UIImage(named: "microphone"), for: .normal)
                        self.button.addTarget(self, action:#selector(self.buttonClicked), for: .touchUpInside)
                        self.buttonCancel.backgroundColor = .clear
                        self.buttonCancel.setImage(UIImage(named: "cancel"), for: .normal)
                        self.buttonCancel.addTarget(self, action:#selector(self.buttonCancelClicked), for: .touchUpInside)
                        self.label.textAlignment = NSTextAlignment.center
                        self.myView.setBorderUIView(color: #colorLiteral(red: 0.0431372549, green: 0.2117647059, blue: 0.3725490196, alpha: 1), borderWidth: 1, cornerRadius: 25)
                        self.label.text = "say".localized
                        self.myView.addSubview(self.button)
                        self.myView.addSubview(self.buttonCancel)
                        self.myView.addSubview(self.label)
                        self.button.isUserInteractionEnabled = false
                        self.label.frame = CGRect(x: self.myView.bounds.width / 2-50,y: (self.myView.bounds.height - 20 - 10),width: 100,height: 20);
                        self.startAudioRecording()
                        self.buttonClicked()
                        if self.workItem.isCancelled {
                            return
                        }
                    }
                    
                case .restricted:
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.finalReportResult.append(Result(name: "Mic".localized, is_worked: false, image: UIImage(named: "mic"), data: nil))
                        Diagnose.GroupDPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                case .notDetermined:
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.finalReportResult.append(Result(name: "Mic".localized, is_worked: false, image: UIImage(named: "mic"), data: nil))
                        Diagnose.GroupDPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                case .denied:
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.finalReportResult.append(Result(name: "Mic".localized, is_worked: false, image: UIImage(named: "mic"), data: nil))
                        Diagnose.GroupDPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                @unknown default:
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        label.addError(view: view)
                        Diagnose.finalReportResult.append(Result(name: "Mic".localized, is_worked: false, image: UIImage(named: "mic"), data: nil))
                        Diagnose.GroupDPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            completion()
                        }
                    }
                }
            }
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
    
    @objc func buttonClicked() {
        if self.workItem.isCancelled {
            return
        }
        self.button.layer.insertSublayer(pulsator, at: 0)
        let buttonMidX = self.button.bounds.midX
            let buttonMidY = self.button.bounds.midY
            let buttonBoundsCenter = CGPoint(x: buttonMidX, y: buttonMidY)
        self.button.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2117647059, blue: 0.3725490196, alpha: 1)
        pulsator.backgroundColor = #colorLiteral(red: 0.04011037946, green: 0.2111303806, blue: 0.3768628538, alpha: 1)
            // 3. set the backgroundLayer's postion to the buttonBoundsCenter
        pulsator.position = buttonBoundsCenter
        pulsator.numPulse = 3
        pulsator.radius = 150

        pulsator.start()
        
        
        
        // Speech To Text section
        SpeechRecognizer = SFSpeechRecognizer(locale: Locale.init(identifier: lang))
        if self.workItem.isCancelled {
            return
        }
        if AudioEngine.isRunning {
            AudioRecorder.stop()
            self.AudioEngine.stop()
            self.RecognitionRequest?.endAudio()
        } else {
            self.startSpeechRecording()
        }
        if self.workItem.isCancelled {
            return
        }
        self.startAudioRecording()
        if self.workItem.isCancelled {
            return
        }
    }
    
    @objc func buttonCancelClicked(){
        if self.workItem.isCancelled {
            return
        }
//        AudioRecorder.stop()
        self.RecognitionRequest = nil
        self.SpeechRecognitionTask = nil
        self.AudioEngine.stop()
        let inputNode = self.AudioEngine.inputNode
        inputNode.removeTap(onBus: 0)
        self.myView.removeFromSuperview()
        Diagnose.finalReportResult.append(Result(name: "Mic".localized, is_worked: false, image: UIImage(named: "mic"), data: nil))
        if self.workItem.isCancelled {
            return
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if self.workItem.isCancelled {
                return
            }
            self.mainLabel.addError(view: self.viewC)
            Diagnose.GroupDPrograss?.value += 3
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                if self.workItem.isCancelled {
                    return
                }
                self.completion()
            }
        }
        
    }
    
    func getDirectory() -> URL
    {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = paths[0]
        return documentDirectory
    }
    
    func startAudioRecording() {
        if self.workItem.isCancelled {
            return
        }
        // Audio Section: If active file saved?
        if AudioRecorder == nil{
            if self.workItem.isCancelled {
                return
            }
            file = 1
            let filename = getDirectory().appendingPathComponent("\(file).m4a")
            let settings = [AVSampleRateKey: 12000,
                            AVNumberOfChannelsKey: 1,
                            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue]
            if self.workItem.isCancelled {
                return
            }
            // Start audio recording
            
            do{
                if self.workItem.isCancelled {
                    return
                }
                AudioRecorder = try AVAudioRecorder(url: filename, settings: settings)
                AudioRecorder.delegate = self.viewC as? AVAudioRecorderDelegate
                AudioRecorder.record()
                
                self.button.backgroundColor = #colorLiteral(red: 0.0431372549, green: 0.2117647059, blue: 0.3725490196, alpha: 1)
                print("recording now")
                if self.workItem.isCancelled {
                    return
                }
            }
            catch
            {
                if self.workItem.isCancelled {
                    return
                }
                self.showAlert(title: "TestMic".localized, message: "MicMessage".localized)
            }
        } else{
            // Stopping audio recording
            if self.workItem.isCancelled {
                return
            }
            AudioRecorder.stop()
            AudioRecorder = nil
            // To save recording
            UserDefaults.standard.set(file, forKey: "recording")
            self.button.backgroundColor = .blue
            
            // Starting playing
            let path = getDirectory().appendingPathComponent("\(file).m4a")
            print("file\(file)")
            if self.workItem.isCancelled {
                return
            }
            
            do
            {
                if self.workItem.isCancelled {
                    return
                }
                AudioPlayer = try AVAudioPlayer(contentsOf: path)
                AudioPlayer.play()
                // Only play once
                AudioPlayer?.numberOfLoops = 0
                // Set the volume of playback here.
                AudioPlayer?.volume = 10.0
                print("playing now")
                if self.workItem.isCancelled {
                    return
                }
            }
            catch
            {
                if self.workItem.isCancelled {
                    return
                }
                self.showAlert(title: "TestMic".localized, message: "MicMessage".localized)
            }
        }
        
    }
    
    func startSpeechRecording() {
        if self.workItem.isCancelled {
            return
        }
        // Cancel the previous task if it's running.
        if SpeechRecognitionTask != nil {
            SpeechRecognitionTask?.cancel()
            SpeechRecognitionTask = nil
        }
        
        RecognitionRequest = SFSpeechAudioBufferRecognitionRequest()
        
        let inputNode = AudioEngine.inputNode
        if self.workItem.isCancelled {
            return
        }
        guard let RecognitionRequest = RecognitionRequest else {
            if self.workItem.isCancelled {
                return
            }
            fatalError("Error in Recognition")
        }
        
        RecognitionRequest.shouldReportPartialResults = true
        if self.workItem.isCancelled {
            return
        }
        SpeechRecognitionTask = SpeechRecognizer?.recognitionTask(with: RecognitionRequest, resultHandler: { (result, error) in
            if self.workItem.isCancelled {
                return
            }
            
            var FinalResult = false
            print(result?.bestTranscription.formattedString ?? "none record")
            if result != nil &&  self.RecognitionRequest != nil && self.SpeechRecognitionTask != nil {
                if self.workItem.isCancelled {
                    return
                }
                print(result?.bestTranscription.formattedString ?? "none record")
                if result!.bestTranscription.formattedString.contains("Hello".localized) || result!.bestTranscription.formattedString.contains("hello".localized){
                    
                    FinalResult = (result?.isFinal)!
                    
                    self.RecognitionRequest = nil
                    self.SpeechRecognitionTask = nil
                    self.AudioEngine.stop()
                    let inputNode = self.AudioEngine.inputNode
                    inputNode.removeTap(onBus: 0)
                    self.myView.removeFromSuperview()
                    if self.workItem.isCancelled {
                        return
                    }
                    
                    Diagnose.finalReportResult.append(Result(name: "Mic".localized, is_worked: true, image: UIImage(named: "mic"), data: [DataResult.init(name: "SpeakerWords".localized, desc: result!.bestTranscription.formattedString)]))
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        if self.workItem.isCancelled {
                            return
                        }
                        self.mainLabel.addSuccess(view: self.viewC)
                        Diagnose.result.append(true)
                        Diagnose.GroupDPrograss?.value += 3
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            if self.workItem.isCancelled {
                                return
                            }
                            self.completion()
                        }
                    }
                }
                
            }
            
            
            if error != nil || FinalResult {
                if self.workItem.isCancelled {
                    return
                }
//                self.AudioRecorder.stop()
                self.RecognitionRequest = nil
                self.SpeechRecognitionTask = nil
                self.AudioEngine.stop()
                inputNode.removeTap(onBus: 0)
                if self.workItem.isCancelled {
                    return
                }
                
            }
        })
        
        let recordingFormat = inputNode.outputFormat(forBus: 0)
        inputNode.installTap(onBus: 0, bufferSize: 1024, format: recordingFormat) { (buffer, when) in
            self.RecognitionRequest?.append(buffer)
        }
        
        AudioEngine.prepare()
        
        do {
            if self.workItem.isCancelled {
                return
            }
            try AudioEngine.start()
            if self.workItem.isCancelled {
                return
            }
        } catch {
            if self.workItem.isCancelled {
                return
            }
            self.showAlert(title: "TestMic".localized, message: "MicMessage".localized)
        }
    }
    
    func speechRecognizer(_ speechRecognizer: SFSpeechRecognizer, availabilityDidChange available: Bool) {
        if available {
            if self.workItem.isCancelled {
                return
            }
            print("speech available")
        } else {
            if self.workItem.isCancelled {
                return
            }
            print("speech not available")
        }
    }
    // To display Alert Message
    func showAlert(title:String, message:String)
    {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "TryAgain".localized, style: .default, handler: {action in
            self.checkMic(view: self.viewC,label: self.mainLabel){
                self.completion()
            }
        }))
        alert.addAction(UIAlertAction(title: "Skip".localized, style: .default, handler: {action in
            if self.workItem.isCancelled {
                return
            }
//            self.AudioRecorder.stop()
            self.RecognitionRequest = nil
            self.SpeechRecognitionTask = nil
            self.AudioEngine.stop()
            let inputNode = self.AudioEngine.inputNode
            inputNode.removeTap(onBus: 0)
            self.myView.removeFromSuperview()
            Diagnose.finalReportResult.append(Result(name: "Mic".localized, is_worked: false, image: UIImage(named: "mic"), data: nil))
            if self.workItem.isCancelled {
                return
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                if self.workItem.isCancelled {
                    return
                }
                self.mainLabel.addError(view: self.viewC)
                Diagnose.GroupDPrograss?.value += 3
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    if self.workItem.isCancelled {
                        return
                    }
                    self.completion()
                }
            }
        }))
        self.viewC.present(alert, animated: true, completion: nil)
    }
}



