//
//  Speaker.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit
import AVFoundation



class Speaker {
    private init() {}
    static let instance = Speaker()
    var randomspeacker = ["00","01","02","03","04","05","06","07","08","09","10"]
    var timesCount = 0
    var workItem: DispatchWorkItem!
    func checkSpeaker(view:UIViewController, label: UILabel,completion: @escaping ()->Void){
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            label.addRunning(view: view)
            
            let alert = UIAlertController(title: "TestSpeaker".localized, message: "SpeakerMessage".localized, preferredStyle: UIAlertController.Style.alert)

                        // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {_ in
                if self.workItem.isCancelled {
                    return
                }
                    do {
                        try AVAudioSession.sharedInstance().setCategory(.playback)
                        let randomnumber = self.randomspeacker.randomElement()!
                        print(randomnumber)
                        let utterance = AVSpeechUtterance(string: randomnumber)
                        utterance.voice = AVSpeechSynthesisVoice(language: "en".localized)
                        if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                            utterance.rate = 0.5
                        }
                        else{
                            utterance.rate = 0.4
                        }
                        

                        let synthesizer = AVSpeechSynthesizer()
                        synthesizer.speak(utterance)
                        if self.workItem.isCancelled {
                            return
                        }
                        
                        let alert2 = UIAlertController(title: "TestSpeaker".localized, message: "SpeakerMessage2".localized, preferredStyle: .alert)

                        //2. Add the text field. You can configure it however you need.
                        alert2.addTextField { (textField) in
                            
    //                        textField.keyboardType = .numberPad
                            textField.keyboardType = .asciiCapableNumberPad
                            textField.textAlignment = .center
                        }
                        

                        // 3. Grab the value from the text field, and print it when the user clicks OK.
                        alert2.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { [weak alert2] (_) in
                            if self.workItem.isCancelled {
                                return
                            }
                            let textField = alert2?.textFields![0]
                            
                            if textField?.text == randomnumber {
                                
                                
                                Diagnose.finalReportResult.append(Result(name: "Speaker".localized, is_worked: true, image: UIImage(named: "speaker"), data: [DataResult(name: "HearedNumber".localized, desc: randomnumber)]))
                                if self.workItem.isCancelled {
                                    return
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                    if self.workItem.isCancelled {
                                        return
                                    }
                                    label.addSuccess(view: view)
                                    Diagnose.result.append(true)
                                    Diagnose.GroupDPrograss?.value += 3
                                    if self.workItem.isCancelled {
                                        return
                                    }
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                        if self.workItem.isCancelled {
                                            return
                                        }
                                        completion()
                                    }
                                }
                            }
                            else{
                                if self.workItem.isCancelled {
                                    return
                                }
                                let alert3 = UIAlertController(title: "TestSpeaker".localized, message: "invalidnumber".localized, preferredStyle: UIAlertController.Style.alert)

                                        // add the actions (buttons)
                                alert3.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: { (_) in
                                    if self.workItem.isCancelled {
                                        return
                                    }
                                    Diagnose.finalReportResult.append(Result(name: "Speaker".localized, is_worked: false, image: UIImage(named: "speaker"), data: nil))
                                    if self.workItem.isCancelled {
                                        return
                                    }
                                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                                if self.workItem.isCancelled {
                                                    return
                                                }
                                                label.addError(view: view)
                                                Diagnose.GroupDPrograss?.value += 3
                                                if self.workItem.isCancelled {
                                                    return
                                                }
                                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                                    if self.workItem.isCancelled {
                                                        return
                                                    }
                                                    completion()
                                                }
                                            }
                                        }))
                                if self.timesCount < 2{
                                    alert3.addAction(UIAlertAction(title: "listenAgain".localized, style: UIAlertAction.Style.cancel, handler: { (_) in
                                        if self.workItem.isCancelled {
                                            return
                                        }
                                        self.checkSpeaker(view: view,label: label){
                                            completion()
                                        }
                                    }))
                                    self.timesCount += 1
                                }
                                        

                                        // show the alert
                                        view.present(alert3, animated: true, completion: nil)
                            }
                        }))
                        
                        if self.timesCount < 2{
                            alert2.addAction(UIAlertAction(title: "listenAgain".localized, style: UIAlertAction.Style.cancel, handler: { (_) in
                                if self.workItem.isCancelled {
                                    return
                                }
                                self.checkSpeaker(view: view,label: label){
                                    completion()
                                }
                            }))
                            self.timesCount += 1
                        }

                        // 4. Present the alert.
                        view.present(alert2, animated: true, completion: nil)
                        
                       } catch(let error) {
                        if self.workItem.isCancelled {
                            return
                        }
                            print(error.localizedDescription)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                label.addError(view: view)
                                Diagnose.GroupDPrograss?.value += 3
                                if self.workItem.isCancelled {
                                    return
                                }
                                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                    if self.workItem.isCancelled {
                                        return
                                    }
                                    completion()
                                }
                            }
                       }
                    
                    
                    }))
                        

                        // show the alert
                        view.present(alert, animated: true, completion: nil)
            
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
}
