//
//  Accelerometer.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit
import CoreMotion

class Accele {
    private init() {}
    static let instance = Accele()
    var motionManager = CMMotionManager()
    var workItem: DispatchWorkItem!
    func checkAccele(view:UIViewController, label: UILabel,completion: @escaping ()->Void) {
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            label.addRunning(view: view)
            let alert = UIAlertController(title: "TestAccelerometer".localized, message: "AccMessage".localized, preferredStyle: UIAlertController.Style.alert)

                    // add an action (button)
            
//            let image = UIImage.gif(name: "ezgif.com-gif-maker.gif")
            let image = UIImageView()
            image.image = UIImage.gif(name: "ezgif.com-gif-maker")
            alert.view.addSubview(image)
            image.translatesAutoresizingMaskIntoConstraints = false
            alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerX, relatedBy: .equal, toItem: alert.view, attribute: .centerX, multiplier: 1, constant: 0))
            alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .centerY, relatedBy: .equal, toItem: alert.view, attribute: .centerY, multiplier: 1, constant: 25))
            alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: alert.view.bounds.width * 0.5))
            alert.view.addConstraint(NSLayoutConstraint(item: image, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: alert.view.bounds.width * 0.25))
            let height:NSLayoutConstraint = NSLayoutConstraint(item: alert.view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 300)
            alert.view.addConstraint(height);
            alert.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: {_ in
                if self.workItem.isCancelled {
                    return
                }
                self.motionManager.stopDeviceMotionUpdates()
                if self.workItem.isCancelled {
                    return
                }
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    if self.workItem.isCancelled {
                        return
                    }
                    label.addError(view: view)
                    Diagnose.GroupCPrograss?.value += 3
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        if self.workItem.isCancelled {
                            return
                        }
                        completion()
                    }
                }

            }))

                    // show the alert
            view.present(alert, animated: true, completion: nil)
            if self.workItem.isCancelled {
                return
            }
            self.motionManager.deviceMotionUpdateInterval = 0.5
            self.motionManager.startDeviceMotionUpdates(to:  OperationQueue.current!) { (data, error) in
                if self.workItem.isCancelled {
                    return
                }
                if let mydata = data {
                    if self.workItem.isCancelled {
                        return
                    }
                    print("Roll: \(mydata.attitude.roll)" )
                    print("Pitch: \(mydata.attitude.pitch)" )
                    print("Yaw: \(mydata.attitude.yaw)" )
                    if (mydata.attitude.roll < -2 || mydata.attitude.pitch < -2 || mydata.attitude.yaw < -2){
                        if self.workItem.isCancelled {
                            return
                        }
                        self.motionManager.stopDeviceMotionUpdates()
                        var isAlertViewPresenting: Bool {
                             get {
                                if view.presentedViewController is UIAlertController {
                                     return true
                                 }
                                return false
                                }
                         }
                        
                        if isAlertViewPresenting{
                            view.dismiss(animated: true, completion: nil)
                        }
                        if self.workItem.isCancelled {
                            return
                        }
                        Diagnose.finalReportResult.append(Result(name: "Accelerometer".localized, is_worked: true, image: UIImage(named: "Accelerometer"), data: nil))
                       
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            label.addSuccess(view: view)
                            Diagnose.result.append(true)
                            Diagnose.GroupCPrograss?.value += 3
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                completion()
                            }
                        }
                        
                    }

                    
                }
                
            }
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
}
