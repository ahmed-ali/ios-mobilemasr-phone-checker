//
//  Vibrate.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit
import AVFoundation
//import AudioToolbox

class Vibrate {
    private init() {}
    static let instance = Vibrate()
    var randomvibrate = [3,4,5]
    var timesCount = 0
    var workItem: DispatchWorkItem!
    func checkVibrate(view:UIViewController, label: UILabel,completion: @escaping ()->Void){
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            label.addRunning(view: view)
            let alert = UIAlertController(title: "TestVibrate".localized, message: "VibrateMessage".localized, preferredStyle: UIAlertController.Style.alert)

                    // add the actions (buttons)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {_ in
                if self.workItem.isCancelled {
                    return
                }
                let randomNum = self.randomvibrate.randomElement()!
                print(randomNum)
                for i in 0...randomNum{
                    if self.workItem.isCancelled {
                        return
                    }
                    DispatchQueue.main.asyncAfter(deadline: .now() + Double(i * 1)/1) {
                        AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    }
                    
                }
                if self.workItem.isCancelled {
                    return
                }
                let alert2 = UIAlertController(title: "TestVibrate".localized, message: "VibrateMessage2".localized, preferredStyle: .alert)

                //2. Add the text field. You can configure it however you need.
                alert2.addTextField { (textField) in
                    textField.keyboardType = .numberPad
                    textField.textAlignment = .center
                }

                // 3. Grab the value from the text field, and print it when the user clicks OK.
                alert2.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { [weak alert2] (_) in
                    if self.workItem.isCancelled {
                    return
                }
                    let textField = alert2?.textFields![0]
                    
                    if textField?.text == String(randomNum + 1) {
                        
                        
                        Diagnose.finalReportResult.append(Result(name: "Vibrate".localized, is_worked: true, image: UIImage(named: "vibration"), data: [DataResult(name: "Count".localized, desc: "\(randomNum + 1)")]))
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            label.addSuccess(view: view)
                            Diagnose.result.append(true)
                            Diagnose.GroupCPrograss?.value += 3
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                completion()
                            }
                        }
                    }
                    else{
                        if self.workItem.isCancelled {
                            return
                        }
                        let alert3 = UIAlertController(title: "TestVibrate".localized, message: "invalidnumber".localized, preferredStyle: UIAlertController.Style.alert)

                                // add the actions (buttons)
                        alert3.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: { (_) in
                            if self.workItem.isCancelled {
                                return
                            }
                            Diagnose.finalReportResult.append(Result(name: "Vibrate".localized, is_worked: false, image: UIImage(named: "vibration"), data: nil))
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        if self.workItem.isCancelled {
                                            return
                                        }
                                        label.addError(view: view)
                                        Diagnose.GroupCPrograss?.value += 3
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                            if self.workItem.isCancelled {
                                                return
                                            }
                                            completion()
                                        }
                                    }
                                }))
                        if self.timesCount < 2 {
                            alert3.addAction(UIAlertAction(title: "VibrateAgain".localized, style: UIAlertAction.Style.cancel, handler: { (_) in
                                if self.workItem.isCancelled {
                                    return
                                }
                                self.checkVibrate(view: view,label: label){
                                    completion()
                                }
                            }))
                            self.timesCount += 1
                        }
                                

                                // show the alert
                                view.present(alert3, animated: true, completion: nil)
                    }
                }))
                
                if self.timesCount < 2 {
                    alert2.addAction(UIAlertAction(title: "VibrateAgain".localized, style: UIAlertAction.Style.cancel, handler: { (_) in
                        if self.workItem.isCancelled {
                            return
                        }
                        self.checkVibrate(view: view,label: label){
                            completion()
                        }
                    }))
                    self.timesCount += 1
                }

                // 4. Present the alert.
                view.present(alert2, animated: true, completion: nil)
            }))
            view.present(alert, animated: true, completion: nil)
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
}
