//
//  Flash.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 16/06/2021.
//

import Foundation
import UIKit
import AVFoundation

class Flash {
    private init() {}
    static let instance = Flash()
    var randomflash = [4,6,8]
    var timesCount = 0
    var workItem: DispatchWorkItem!
    let device = AVCaptureDevice.default(for: AVMediaType.video)
    func checkFlash(view:UIViewController, label: UILabel,completion: @escaping ()->Void){
        self.workItem = DispatchWorkItem {
            if self.workItem.isCancelled {
                return
            }
            label.addRunning(view: view)
            let alert = UIAlertController(title: "TestFlash".localized, message: "FlashMessage".localized, preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Continue".localized, style: UIAlertAction.Style.default, handler: {_ in
                if self.workItem.isCancelled {
                    return
                }
                let randomNum = self.randomflash.randomElement()!
                print(randomNum)
                
                var count = 0
                if #available(iOS 10.0, *) {
                    _ = Timer.scheduledTimer(withTimeInterval: 1, repeats: true){ t in
                        count += 1
                        do {
                            
                            try self.device?.lockForConfiguration()
                            try self.device?.setTorchModeOn(level: 1.0)
                            
                            self.device?.torchMode = self.device!.isTorchActive ? .off : .on
                            self.device?.unlockForConfiguration()
                            
                        } catch {
                            assert(false, "error: device flash light, \(error)")
                        }
                        print(count)
                        if count >= randomNum {
                            t.invalidate()
                        }
                    }
                }
                if self.workItem.isCancelled {
                    return
                }
                let alert2 = UIAlertController(title: "TestFlash".localized, message: "FlashMessage2".localized, preferredStyle: .alert)

                //2. Add the text field. You can configure it however you need.
                alert2.addTextField { (textField) in
                    textField.keyboardType = .numberPad
                    textField.textAlignment = .center
                }

                // 3. Grab the value from the text field, and print it when the user clicks OK.
                alert2.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { [weak alert2] (_) in
                    if self.workItem.isCancelled {
                        return
                    }
                    let textField = alert2?.textFields![0]
                    
                    if textField?.text == String(randomNum / 2) {
                        
                        
                        Diagnose.finalReportResult.append(Result(name: "Flash".localized, is_worked: true, image: UIImage(named: "flash"), data: [DataResult(name: "Count".localized, desc: "\(randomNum / 2)")]))
                        if self.workItem.isCancelled {
                            return
                        }
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                            if self.workItem.isCancelled {
                                return
                            }
                            label.addSuccess(view: view)
                            Diagnose.result.append(true)
                            Diagnose.GroupDPrograss?.value += 3
                            if self.workItem.isCancelled {
                                return
                            }
                            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                if self.workItem.isCancelled {
                                    return
                                }
                                completion()
                            }
                        }
                    }
                    else{
                        if self.workItem.isCancelled {
                            return
                        }
                        let alert3 = UIAlertController(title: "TestFlash".localized, message: "invalidnumber".localized, preferredStyle: UIAlertController.Style.alert)

                                // add the actions (buttons)
                        alert3.addAction(UIAlertAction(title: "Skip".localized, style: UIAlertAction.Style.default, handler: { (_) in
                            if self.workItem.isCancelled {
                                return
                            }
                            Diagnose.finalReportResult.append(Result(name: "Flash".localized, is_worked: false, image: UIImage(named: "flash"), data: nil))
                            if self.workItem.isCancelled {
                                return
                            }
                                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                                        label.addError(view: view)
                                        Diagnose.GroupDPrograss?.value += 3
                                        if self.workItem.isCancelled {
                                            return
                                        }
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                                            if self.workItem.isCancelled {
                                                return
                                            }
                                            completion()
                                        }
                                    }
                                }))
                        if self.timesCount < 2 {
                            alert3.addAction(UIAlertAction(title: "FlashAgain".localized, style: UIAlertAction.Style.cancel, handler: { (_) in
                                if self.workItem.isCancelled {
                                    return
                                }
                                self.checkFlash(view: view,label: label){
                                    completion()
                                }
                            }))
                            self.timesCount += 1
                        }
                                

                                // show the alert
                                view.present(alert3, animated: true, completion: nil)
                    }
                }))
                
                if self.timesCount < 2{
                    alert2.addAction(UIAlertAction(title: "FlashAgain".localized, style: UIAlertAction.Style.cancel, handler: { (_) in
                        if self.workItem.isCancelled {
                            return
                        }
                        self.checkFlash(view: view,label: label){
                            completion()
                        }
                    }))
                    self.timesCount += 1
                }

                // 4. Present the alert.
                view.present(alert2, animated: true, completion: nil)
            }))
            view.present(alert, animated: true, completion: nil)
        }
        DispatchQueue.main.async(execute: self.workItem)
    }
}
