//
//  UILabelExtensions.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 19/08/2021.
//

import Foundation
import UIKit
import AVFoundation
import SwiftGifOrigin

extension UILabel{
    func addRunning(view:UIViewController){
        self.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
            let imageAttachment = NSTextAttachment()
            imageAttachment.image = UIImage(named:"settings")
            
            // Set bound to reposition
            let imageOffsetY: CGFloat = -5.0
            if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                imageAttachment.bounds = CGRect(x: 0 , y: imageOffsetY, width: 20, height: 20)
            }
            else{
                imageAttachment.bounds = CGRect(x: 5 , y: imageOffsetY, width: 20, height: 20)
            }
            
            // Create string with attachment
            let attachmentString = NSAttributedString(attachment: imageAttachment)
            // Initialize mutable string
            let completeText = NSMutableAttributedString(string: "")
            // Add image to mutable string
            completeText.append(attachmentString)
            // Add your text to mutable string
            let textAfterIcon = NSAttributedString(string: "Running".localized)
            completeText.append(textAfterIcon)
            self.attributedText = completeText
            view.view.layoutIfNeeded()
            self.alpha = 1
        }, completion: nil)
    }
    func addSuccess(view:UIViewController){
        
        self.alpha = 0
        Diagnose.prograssCount += 3
            UIView.animate(withDuration: 0.5, delay: 0.5, options: [.curveEaseOut], animations: {
                let imageAttachment = NSTextAttachment()
                imageAttachment.image = UIImage(named:"check")
                // Set bound to reposition
                let imageOffsetY: CGFloat = -5.0
                if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    imageAttachment.bounds = CGRect(x: 0 , y: imageOffsetY, width: 20, height: 20)
                }
                else{
                    imageAttachment.bounds = CGRect(x: 5 , y: imageOffsetY, width: 20, height: 20)
                }
                // Create string with attachment
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                // Initialize mutable string
                let completeText = NSMutableAttributedString(string: "")
                // Add image to mutable string
                completeText.append(attachmentString)
                self.attributedText = completeText
                view.view.layoutIfNeeded()
                self.alpha = 1
            }, completion: nil)
        
        
        
    }
    func addError(view:UIViewController){
        self.alpha = 0
        Diagnose.prograssCount += 3
            UIView.animate(withDuration: 0.5, delay: 0, options: [.curveEaseOut], animations: {
                let imageAttachment = NSTextAttachment()
                imageAttachment.image = UIImage(named:"x-mark")
                // Set bound to reposition
                let imageOffsetY: CGFloat = -5.0
                if LocalizationSystem.sharedInstance.getLanguage() == "ar" {
                    imageAttachment.bounds = CGRect(x: 0 , y: imageOffsetY, width: 20, height: 20)
                }
                else{
                    imageAttachment.bounds = CGRect(x: 5 , y: imageOffsetY, width: 20, height: 20)
                }
                // Create string with attachment
                let attachmentString = NSAttributedString(attachment: imageAttachment)
                // Initialize mutable string
                let completeText = NSMutableAttributedString(string: "")
                // Add image to mutable string
                completeText.append(attachmentString)
                self.attributedText = completeText
                view.view.layoutIfNeeded()
                self.alpha = 1
            }, completion: nil)
        
        
        
    }
    func addText(view:UIViewController,text:String){
        self.alpha = 0
        Diagnose.prograssCount += 3
            UIView.animate(withDuration: 0.5, delay: 0.5, options: [.curveEaseOut], animations: {
                // Initialize mutable string
                let completeText = NSMutableAttributedString(string: "")
                // Add your text to mutable string
                let textAfterIcon = NSAttributedString(string:text)
                completeText.append(textAfterIcon)
                self.attributedText = completeText
                view.view.layoutIfNeeded()
                self.alpha = 1
            }, completion: nil)
        
        
    }
    
    func setTyping(text: String, characterDelay: TimeInterval = 5.0) {
      self.text = ""
        
      let writingTask = DispatchWorkItem { [weak self] in
        text.forEach { char in
          DispatchQueue.main.async {
            self?.text?.append(char)
          }
          Thread.sleep(forTimeInterval: characterDelay/100)
        }
      }
        
      let queue: DispatchQueue = .init(label: "typespeed", qos: .userInteractive)
      queue.asyncAfter(deadline: .now() + 0.05, execute: writingTask)
    }
}
