//
//  UITextFieldExtensions.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 19/08/2021.
//

import Foundation
import UIKit
extension UITextField: UITextFieldDelegate {
    
    enum Direction {
        case Left
        case Right
    }
    
    // add image to textfield
    func withImage(direction: Direction, image: String, colorBorder: UIColor , cornerRadius: CGFloat, imageColor: UIColor, placeholder : String){
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: 50, height: 45))
        mainView.layer.cornerRadius = 5
        
        let imageView = UIImageView(image: UIImage(named: image))
        imageView.tintColor = imageColor
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: 12.0, y: 10.0, width: 24.0, height: 24.0)
        mainView.addSubview(imageView)
        
        self.placeholder = placeholder
        
        if(Direction.Left == direction){ // image left
            
            self.leftViewMode = .always
            self.leftView = mainView
        } else { // image right
            self.rightViewMode = .always
            self.rightView = mainView
        }
        
        self.layer.borderColor = colorBorder.cgColor
        self.layer.borderWidth = CGFloat(1)
        self.layer.cornerRadius = cornerRadius
    }
    
   func addInputAccessoryView(title: String, target: Any, selector: Selector) {
       
       let toolBar = UIToolbar(frame: CGRect(x: 0.0,
                                             y: 0.0,
                                             width: UIScreen.main.bounds.size.width,
                                             height: 44.0))//1
       let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)//2
       let barButton = UIBarButtonItem(title: title, style: .plain, target: target, action: selector)//3
       toolBar.setItems([flexible, barButton], animated: false)//4
       self.inputAccessoryView = toolBar//5
   }
    
    
}
