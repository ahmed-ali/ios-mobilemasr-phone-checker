//
//  StringExtensions.swift
//  MobileMasr
//
//  Created by Ahmed Ali on 19/08/2021.
//

import Foundation
import UIKit
import AVFoundation
import SwiftGifOrigin


extension String {
    var localized : String {
        return NSLocalizedString(self, comment: "")
    }
    var htmlToAttributedString: NSAttributedString? {
            guard let data = data(using: .utf8) else { return nil }
            do {
                return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            } catch {
                return nil
            }
        }
        var htmlToString: String {
            return htmlToAttributedString?.string ?? ""
        }
    
    func index(at location: Int) -> String.Index {
        return self.index(self.startIndex, offsetBy: location)
    }
}
